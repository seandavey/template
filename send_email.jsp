<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="app.Request,app.Site,util.Text,db.Rows,db.Select" %>
<%
	Request r = new Request(request, response, out);
	Site.site.pageOpen("", true, r);
%>
	<h3>Send Email to Individual Community Members</h3>
	<%
		r.w.ui.buttonOutlineOnClick("Select All", "cb_all(true);clicked()")
			.addStyle("margin-left:10px")
			.ui.buttonOutlineOnClick("Select None", "cb_all(false);clicked()");
	%>
	<div style="display:flex;flex-wrap:wrap;">
<%
	Rows rows = new Rows(new Select("first,last,email").from("people").where("active AND email IS NOT NULL AND NOT hide_email_on_site").orderBy("first,last"), r.db);
	while (rows.next()) {
		String name = Text.join(" ", rows.getString(1), rows.getString(2));
		r.w.write("<div style=\"width:200px\">")
			.setAttribute("email", rows.getString(3))
			.setAttribute("onclick", "clicked()")
			.ui.checkbox(name, name, null, false, false, false)
			.write("</div>");
	}
	rows.close();
%>
	</div>
	<br/>
	<%
		r.w.setId("compose")
			.ui.aButton("Compose Message", "javascript:Dialog.alert(null,'Please select one or more recipients.')");
	%>
	<script>
		function cb_all(checked) {
			var inputs = _.$$("input[type='checkbox']")
			inputs.forEach(function(cb) {
				cb.checked = checked
			})
		}
		function clicked() {
			var addresses = ''
			var inputs = _.$$("input[type='checkbox']")
			inputs.forEach(function(cb) {
				if (cb.checked) {
					if (addresses)
						addresses += ','
					addresses += cb.getAttribute('email')
				}
			})
			_.$('#compose').href = addresses ? 'mailto:' + addresses : "javascript:Dialog.alert(null,'Please select one or more recipients.')"
		}
	</script>
<%
	r.close();
%>
