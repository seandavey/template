--
-- PostgreSQL database dump
--

-- Dumped from database version 14.12 (Homebrew)
-- Dumped by pg_dump version 14.12 (Homebrew)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: naturalsort(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.naturalsort(text) RETURNS bytea
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
    select string_agg(convert_to(coalesce(r[2], length(length(r[1])::text) || length(r[1])::text || r[1]), 'SQL_ASCII'),'\x00')
    from regexp_matches($1, '0*([0-9]+)|([^0-9]+)', 'g') r;
$_$;


ALTER FUNCTION public.naturalsort(text) OWNER TO postgres;

--
-- Name: naturalsort(character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.naturalsort(character varying) RETURNS bytea
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
select string_agg(convert_to(coalesce(r[2], length(length(r[1])::text) || length(r[1])::text || r[1]), 'SQL_ASCII'),'\\x00')
from regexp_matches($1, '0*([0-9]+)|([^0-9]+)', 'g') r;
$_$;


ALTER FUNCTION public.naturalsort(character varying) OWNER TO postgres;

--
-- Name: concat(text); Type: AGGREGATE; Schema: public; Owner: postgres
--

CREATE AGGREGATE public.concat(text) (
    SFUNC = textcat,
    STYPE = text,
    INITCOND = ''
);


ALTER AGGREGATE public.concat(text) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: _columns_; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._columns_ (
    id integer NOT NULL,
    _views__id integer,
    name character varying,
    display_name character varying,
    display_length integer,
    is_hidden boolean DEFAULT false,
    is_read_only boolean DEFAULT false,
    is_required boolean DEFAULT false,
    type character varying,
    type_args character varying,
    is_unique boolean DEFAULT false,
    post_text character varying,
    pre_text character varying,
    show_previous_values_dropdown boolean DEFAULT false,
    title character varying,
    json character varying,
    cell_style character varying,
    column_head character varying,
    align_right boolean DEFAULT false
);


ALTER TABLE public._columns_ OWNER TO postgres;

--
-- Name: _columns__id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._columns__id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._columns__id_seq OWNER TO postgres;

--
-- Name: _columns__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._columns__id_seq OWNED BY public._columns_.id;


--
-- Name: _modules_; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._modules_ (
    id integer NOT NULL,
    class character varying,
    name character varying,
    json jsonb
);


ALTER TABLE public._modules_ OWNER TO postgres;

--
-- Name: _modules__id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._modules__id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._modules__id_seq OWNER TO postgres;

--
-- Name: _modules__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._modules__id_seq OWNED BY public._modules_.id;


--
-- Name: _relationship_defs_; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._relationship_defs_ (
    id integer NOT NULL,
    _views__id integer,
    many_view_def_name character varying,
    many_table_column character varying
);


ALTER TABLE public._relationship_defs_ OWNER TO postgres;

--
-- Name: _relationship_defs__id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._relationship_defs__id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._relationship_defs__id_seq OWNER TO postgres;

--
-- Name: _relationship_defs__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._relationship_defs__id_seq OWNED BY public._relationship_defs_.id;


--
-- Name: _roles_; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._roles_ (
    id integer NOT NULL,
    role character varying
);


ALTER TABLE public._roles_ OWNER TO postgres;

--
-- Name: _roles__id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._roles__id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._roles__id_seq OWNER TO postgres;

--
-- Name: _roles__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._roles__id_seq OWNED BY public._roles_.id;


--
-- Name: _settings_; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._settings_ (
    id integer NOT NULL,
    name character varying,
    value character varying
);


ALTER TABLE public._settings_ OWNER TO postgres;

--
-- Name: _settings__id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._settings__id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._settings__id_seq OWNER TO postgres;

--
-- Name: _settings__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._settings__id_seq OWNED BY public._settings_.id;


--
-- Name: _tables_; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._tables_ (
    id integer NOT NULL,
    name character varying,
    json character varying
);


ALTER TABLE public._tables_ OWNER TO postgres;

--
-- Name: _tables__id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._tables__id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._tables__id_seq OWNER TO postgres;

--
-- Name: _tables__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._tables__id_seq OWNED BY public._tables_.id;


--
-- Name: _views_; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public._views_ (
    id integer NOT NULL,
    name character varying,
    allow_quick_edit boolean DEFAULT false,
    column_names_form text,
    column_names_form_table text,
    column_names_table text,
    default_order_by character varying,
    "from" character varying,
    record_name character varying,
    row_window_size integer,
    add_button_text character varying,
    show_num_records boolean DEFAULT false,
    filters character varying,
    base_filter character varying,
    column_names_all character varying,
    show_table_column_picker boolean DEFAULT false,
    delete_button_text character varying,
    replace_on_quick_edit boolean DEFAULT false,
    after_form character varying,
    column_heads_can_wrap boolean DEFAULT false,
    edit_button_text character varying
);


ALTER TABLE public._views_ OWNER TO postgres;

--
-- Name: _views__id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public._views__id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public._views__id_seq OWNER TO postgres;

--
-- Name: _views__id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public._views__id_seq OWNED BY public._views_.id;


--
-- Name: accounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accounts (
    id integer NOT NULL,
    name character varying,
    type integer,
    email character varying,
    initial_balance double precision,
    ongoing_account integer,
    active boolean DEFAULT true
);


ALTER TABLE public.accounts OWNER TO postgres;

--
-- Name: accounts_budgets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.accounts_budgets (
    id integer NOT NULL,
    accounts_id integer,
    year integer,
    amount double precision
);


ALTER TABLE public.accounts_budgets OWNER TO postgres;

--
-- Name: accounts_budgets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.accounts_budgets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accounts_budgets_id_seq OWNER TO postgres;

--
-- Name: accounts_budgets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.accounts_budgets_id_seq OWNED BY public.accounts_budgets.id;


--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accounts_id_seq OWNER TO postgres;

--
-- Name: accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.accounts_id_seq OWNED BY public.accounts.id;


--
-- Name: additional_emails; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.additional_emails (
    id integer NOT NULL,
    people_id integer,
    email character varying
);


ALTER TABLE public.additional_emails OWNER TO postgres;

--
-- Name: additional_emails_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.additional_emails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.additional_emails_id_seq OWNER TO postgres;

--
-- Name: additional_emails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.additional_emails_id_seq OWNED BY public.additional_emails.id;


--
-- Name: additional_senders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.additional_senders (
    id integer NOT NULL,
    name character varying,
    email character varying
);


ALTER TABLE public.additional_senders OWNER TO postgres;

--
-- Name: additional_senders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.additional_senders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.additional_senders_id_seq OWNER TO postgres;

--
-- Name: additional_senders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.additional_senders_id_seq OWNED BY public.additional_senders.id;


--
-- Name: automatic_transactions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.automatic_transactions (
    id integer NOT NULL,
    accounts_id integer,
    amount double precision,
    bank_accounts_id integer,
    description character varying,
    households_id integer,
    generate_day integer,
    transaction_day integer,
    type character varying,
    payees_id integer
);


ALTER TABLE public.automatic_transactions OWNER TO postgres;

--
-- Name: automatic_transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.automatic_transactions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.automatic_transactions_id_seq OWNER TO postgres;

--
-- Name: automatic_transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.automatic_transactions_id_seq OWNED BY public.automatic_transactions.id;


--
-- Name: bank_accounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bank_accounts (
    id integer NOT NULL,
    name character varying,
    starting_balance double precision DEFAULT 0,
    reconcile boolean DEFAULT true
);


ALTER TABLE public.bank_accounts OWNER TO postgres;

--
-- Name: bank_accounts_balances; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bank_accounts_balances (
    id integer NOT NULL,
    amount double precision,
    bank_accounts_id integer,
    date date
);


ALTER TABLE public.bank_accounts_balances OWNER TO postgres;

--
-- Name: bank_accounts_balances_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bank_accounts_balances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_accounts_balances_id_seq OWNER TO postgres;

--
-- Name: bank_accounts_balances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bank_accounts_balances_id_seq OWNED BY public.bank_accounts_balances.id;


--
-- Name: bank_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bank_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bank_accounts_id_seq OWNER TO postgres;

--
-- Name: bank_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bank_accounts_id_seq OWNED BY public.bank_accounts.id;


--
-- Name: bicycles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bicycles (
    id integer NOT NULL,
    households_id integer,
    available_for_sharing boolean DEFAULT false,
    description character varying,
    notes character varying,
    parking_spot character varying,
    photo character varying
);


ALTER TABLE public.bicycles OWNER TO postgres;

--
-- Name: bicycles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bicycles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bicycles_id_seq OWNER TO postgres;

--
-- Name: bicycles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bicycles_id_seq OWNED BY public.bicycles.id;


--
-- Name: birthday_reminders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.birthday_reminders (
    id integer NOT NULL,
    before boolean DEFAULT true,
    email character varying,
    note character varying,
    num integer,
    _owner_ integer,
    repeat_days character varying DEFAULT 'each event'::character varying,
    unit character varying
);


ALTER TABLE public.birthday_reminders OWNER TO postgres;

--
-- Name: birthday_reminders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.birthday_reminders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.birthday_reminders_id_seq OWNER TO postgres;

--
-- Name: birthday_reminders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.birthday_reminders_id_seq OWNED BY public.birthday_reminders.id;


--
-- Name: birthdays_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.birthdays_events (
    id integer NOT NULL,
    date date,
    event character varying
);


ALTER TABLE public.birthdays_events OWNER TO postgres;

--
-- Name: birthdays_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.birthdays_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.birthdays_events_id_seq OWNER TO postgres;

--
-- Name: birthdays_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.birthdays_events_id_seq OWNED BY public.birthdays_events.id;


--
-- Name: blackboard; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.blackboard (
    id integer NOT NULL,
    text character varying,
    _timestamp_ timestamp without time zone
);


ALTER TABLE public.blackboard OWNER TO postgres;

--
-- Name: blackboard_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.blackboard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blackboard_id_seq OWNER TO postgres;

--
-- Name: blackboard_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.blackboard_id_seq OWNED BY public.blackboard.id;


--
-- Name: budgets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.budgets (
    id integer NOT NULL,
    account character varying,
    type integer,
    email character varying,
    initial_balance real
);


ALTER TABLE public.budgets OWNER TO postgres;

--
-- Name: budgets_amounts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.budgets_amounts (
    id integer NOT NULL,
    budgets_id integer,
    year integer,
    amount real
);


ALTER TABLE public.budgets_amounts OWNER TO postgres;

--
-- Name: budgets_amounts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.budgets_amounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.budgets_amounts_id_seq OWNER TO postgres;

--
-- Name: budgets_amounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.budgets_amounts_id_seq OWNED BY public.budgets_amounts.id;


--
-- Name: budgets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.budgets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.budgets_id_seq OWNER TO postgres;

--
-- Name: budgets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.budgets_id_seq OWNED BY public.budgets.id;


--
-- Name: calendar_guest_room_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.calendar_guest_room_events (
    id integer NOT NULL,
    date date,
    notes character varying,
    _owner_ integer,
    repeat character varying DEFAULT 'never'::character varying,
    end_date date,
    event_id integer,
    event character varying,
    _timestamp_ timestamp without time zone
);


ALTER TABLE public.calendar_guest_room_events OWNER TO postgres;

--
-- Name: calendar_guest_room_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.calendar_guest_room_events_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calendar_guest_room_events_id_seq OWNER TO postgres;

--
-- Name: calendar_guest_room_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.calendar_guest_room_events_id_seq OWNED BY public.calendar_guest_room_events.id;


--
-- Name: contacts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.contacts (
    id integer NOT NULL,
    households_id integer,
    first character varying,
    last character varying,
    relationship character varying,
    phone character varying,
    email character varying,
    address character varying,
    notes character varying,
    public boolean DEFAULT false,
    category character varying,
    groups_id integer
);


ALTER TABLE public.contacts OWNER TO postgres;

--
-- Name: contacts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.contacts_id_seq OWNER TO postgres;

--
-- Name: contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.contacts_id_seq OWNED BY public.contacts.id;


--
-- Name: decisions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.decisions (
    id integer NOT NULL,
    date date,
    title character varying,
    text character varying,
    status character varying,
    type character varying DEFAULT 'Regular'::character varying,
    _owner_ integer,
    _timestamp_ timestamp without time zone,
    groups_id integer,
    tsvector tsvector,
    likes character varying,
    show_on_news_feed boolean DEFAULT true
);


ALTER TABLE public.decisions OWNER TO postgres;

--
-- Name: decisions_agree; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.decisions_agree (
    id integer NOT NULL,
    decisions_id integer,
    people_id integer
);


ALTER TABLE public.decisions_agree OWNER TO postgres;

--
-- Name: decisions_agree_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.decisions_agree_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.decisions_agree_id_seq OWNER TO postgres;

--
-- Name: decisions_agree_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.decisions_agree_id_seq OWNED BY public.decisions_agree.id;


--
-- Name: decisions_attachments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.decisions_attachments (
    id integer NOT NULL,
    decisions_id integer,
    filename character varying
);


ALTER TABLE public.decisions_attachments OWNER TO postgres;

--
-- Name: decisions_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.decisions_attachments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.decisions_attachments_id_seq OWNER TO postgres;

--
-- Name: decisions_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.decisions_attachments_id_seq OWNED BY public.decisions_attachments.id;


--
-- Name: decisions_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.decisions_comments (
    id integer NOT NULL,
    decisions_id integer,
    reply_to integer,
    comment character varying,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.decisions_comments OWNER TO postgres;

--
-- Name: decisions_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.decisions_comments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.decisions_comments_id_seq OWNER TO postgres;

--
-- Name: decisions_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.decisions_comments_id_seq OWNED BY public.decisions_comments.id;


--
-- Name: decisions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.decisions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.decisions_id_seq OWNER TO postgres;

--
-- Name: decisions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.decisions_id_seq OWNED BY public.decisions.id;


--
-- Name: decisions_rank; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.decisions_rank (
    id integer NOT NULL,
    decisions_id integer,
    people_id integer,
    _order_ integer
);


ALTER TABLE public.decisions_rank OWNER TO postgres;

--
-- Name: decisions_rank_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.decisions_rank_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.decisions_rank_id_seq OWNER TO postgres;

--
-- Name: decisions_rank_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.decisions_rank_id_seq OWNED BY public.decisions_rank.id;


--
-- Name: discussions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discussions (
    id integer NOT NULL,
    title character varying,
    text text,
    _timestamp_ timestamp without time zone,
    _owner_ integer,
    likes character varying,
    show_on_news_feed boolean DEFAULT true,
    discussions_categories_id integer,
    close_date date
);


ALTER TABLE public.discussions OWNER TO postgres;

--
-- Name: discussions_attachments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discussions_attachments (
    id integer NOT NULL,
    discussions_id integer,
    filename character varying
);


ALTER TABLE public.discussions_attachments OWNER TO postgres;

--
-- Name: discussions_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discussions_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discussions_attachments_id_seq OWNER TO postgres;

--
-- Name: discussions_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discussions_attachments_id_seq OWNED BY public.discussions_attachments.id;


--
-- Name: discussions_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discussions_categories (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.discussions_categories OWNER TO postgres;

--
-- Name: discussions_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discussions_categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discussions_categories_id_seq OWNER TO postgres;

--
-- Name: discussions_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discussions_categories_id_seq OWNED BY public.discussions_categories.id;


--
-- Name: discussions_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.discussions_comments (
    id integer NOT NULL,
    discussions_id integer,
    reply_to integer,
    comment text,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.discussions_comments OWNER TO postgres;

--
-- Name: discussions_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discussions_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discussions_comments_id_seq OWNER TO postgres;

--
-- Name: discussions_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discussions_comments_id_seq OWNED BY public.discussions_comments.id;


--
-- Name: discussions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.discussions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.discussions_id_seq OWNER TO postgres;

--
-- Name: discussions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.discussions_id_seq OWNED BY public.discussions.id;


--
-- Name: documents; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documents (
    id integer NOT NULL,
    filename character varying,
    title character varying,
    type character varying,
    groups_id integer,
    show_on_main_docs_page boolean DEFAULT true,
    _timestamp_ timestamp without time zone,
    show_on_news_feed boolean DEFAULT true,
    tsvector tsvector,
    _owner_ integer,
    folder integer,
    kind character(1)
);


ALTER TABLE public.documents OWNER TO postgres;

--
-- Name: documents_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documents_comments (
    id integer NOT NULL,
    documents_id integer,
    reply_to integer,
    comment character varying,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.documents_comments OWNER TO postgres;

--
-- Name: documents_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documents_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documents_comments_id_seq OWNER TO postgres;

--
-- Name: documents_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.documents_comments_id_seq OWNED BY public.documents_comments.id;


--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documents_id_seq OWNER TO postgres;

--
-- Name: documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.documents_id_seq OWNED BY public.documents.id;


--
-- Name: documents_types; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documents_types (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.documents_types OWNER TO postgres;

--
-- Name: documents_types_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documents_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documents_types_id_seq OWNER TO postgres;

--
-- Name: documents_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.documents_types_id_seq OWNED BY public.documents_types.id;


--
-- Name: equipment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.equipment (
    id integer NOT NULL,
    item character varying,
    location character varying,
    notes character varying
);


ALTER TABLE public.equipment OWNER TO postgres;

--
-- Name: equipment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.equipment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.equipment_id_seq OWNER TO postgres;

--
-- Name: equipment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.equipment_id_seq OWNED BY public.equipment.id;


--
-- Name: general_meetings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.general_meetings (
    id integer NOT NULL,
    agenda character varying,
    date date,
    end_time time without time zone,
    start_time time without time zone,
    facilitator integer,
    annual_meeting boolean DEFAULT false,
    budget_meeting boolean DEFAULT false
);


ALTER TABLE public.general_meetings OWNER TO postgres;

--
-- Name: general_meetings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.general_meetings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.general_meetings_id_seq OWNER TO postgres;

--
-- Name: general_meetings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.general_meetings_id_seq OWNED BY public.general_meetings.id;


--
-- Name: group_links; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.group_links (
    id integer NOT NULL,
    groups_id integer,
    title character varying,
    url text,
    _timestamp_ timestamp without time zone
);


ALTER TABLE public.group_links OWNER TO postgres;

--
-- Name: group_links_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.group_links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group_links_id_seq OWNER TO postgres;

--
-- Name: group_links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.group_links_id_seq OWNED BY public.group_links.id;


--
-- Name: groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.groups (
    id integer NOT NULL,
    name character varying,
    mandate character varying,
    calendar boolean DEFAULT false,
    self_joining boolean DEFAULT true,
    active boolean DEFAULT true,
    calendar_public boolean DEFAULT false,
    role character varying,
    calendar_access_policy character varying DEFAULT 'group members'::character varying,
    private boolean DEFAULT false,
    items character varying
);


ALTER TABLE public.groups OWNER TO postgres;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO postgres;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- Name: groups_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.groups_jobs (
    id integer NOT NULL,
    groups_id integer,
    people_id integer,
    job character varying,
    start date,
    "end" date
);


ALTER TABLE public.groups_jobs OWNER TO postgres;

--
-- Name: groups_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.groups_jobs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_jobs_id_seq OWNER TO postgres;

--
-- Name: groups_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.groups_jobs_id_seq OWNED BY public.groups_jobs.id;


--
-- Name: guest_room_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.guest_room_events (
    id integer NOT NULL,
    date date,
    _owner_ integer,
    repeat character varying,
    end_date date,
    event character varying,
    _timestamp_ timestamp without time zone,
    notes character varying,
    event_id integer
);


ALTER TABLE public.guest_room_events OWNER TO postgres;

--
-- Name: guest_room_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.guest_room_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guest_room_events_id_seq OWNER TO postgres;

--
-- Name: guest_room_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.guest_room_events_id_seq OWNED BY public.guest_room_events.id;


--
-- Name: homes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.homes (
    id integer NOT NULL,
    number character varying,
    units_id integer,
    pays_hoa integer,
    pays_water integer
);


ALTER TABLE public.homes OWNER TO postgres;

--
-- Name: homes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.homes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.homes_id_seq OWNER TO postgres;

--
-- Name: homes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.homes_id_seq OWNED BY public.homes.id;


--
-- Name: households; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.households (
    id integer DEFAULT nextval(('public.households_id_seq'::text)::regclass) NOT NULL,
    name character varying,
    contact integer,
    cm_account_active boolean DEFAULT true,
    meal_work_exempt boolean DEFAULT false,
    homes_id integer,
    active boolean DEFAULT true,
    bio character varying,
    emergency_info character varying
);


ALTER TABLE public.households OWNER TO postgres;

--
-- Name: households_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.households_events (
    id integer DEFAULT nextval(('public.households_events_id_seq'::text)::regclass) NOT NULL,
    date date,
    notes character varying,
    _owner_ integer,
    repeat character varying,
    end_date date,
    event_id integer,
    event character varying,
    start_time time without time zone,
    end_time time without time zone,
    _timestamp_ timestamp without time zone,
    households_id integer
);


ALTER TABLE public.households_events OWNER TO postgres;

--
-- Name: households_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.households_events_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.households_events_id_seq OWNER TO postgres;

--
-- Name: households_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.households_events_id_seq OWNED BY public.households_events.id;


--
-- Name: households_events_reminders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.households_events_reminders (
    id integer DEFAULT nextval(('public.households_events_reminders_id_seq'::text)::regclass) NOT NULL,
    households_events_id integer,
    before boolean DEFAULT true,
    email character varying,
    note character varying,
    num integer,
    _owner_ integer,
    repeat_days character varying DEFAULT 'each event'::character varying,
    unit character varying
);


ALTER TABLE public.households_events_reminders OWNER TO postgres;

--
-- Name: households_events_reminders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.households_events_reminders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.households_events_reminders_id_seq OWNER TO postgres;

--
-- Name: households_events_reminders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.households_events_reminders_id_seq OWNED BY public.households_events_reminders.id;


--
-- Name: households_homes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.households_homes (
    households_id integer,
    homes_id integer
);


ALTER TABLE public.households_homes OWNER TO postgres;

--
-- Name: households_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.households_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.households_id_seq OWNER TO postgres;

--
-- Name: households_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.households_id_seq OWNED BY public.households.id;


--
-- Name: households_pictures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.households_pictures (
    id integer DEFAULT nextval(('public.households_pictures_id_seq'::text)::regclass) NOT NULL,
    households_id integer,
    filename character varying
);


ALTER TABLE public.households_pictures OWNER TO postgres;

--
-- Name: households_pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.households_pictures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.households_pictures_id_seq OWNER TO postgres;

--
-- Name: households_pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.households_pictures_id_seq OWNED BY public.households_pictures.id;


--
-- Name: jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jobs (
    id integer NOT NULL,
    job character varying,
    description character varying,
    notes character varying,
    term_type character varying DEFAULT 'annual'::character varying,
    groups_id integer,
    num_people_needed integer
);


ALTER TABLE public.jobs OWNER TO postgres;

--
-- Name: jobs_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jobs_history (
    id integer NOT NULL,
    person integer,
    start date,
    "end" date,
    jobs_id integer,
    notes character varying,
    start_date date,
    end_date date
);


ALTER TABLE public.jobs_history OWNER TO postgres;

--
-- Name: jobs_history_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jobs_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jobs_history_id_seq OWNER TO postgres;

--
-- Name: jobs_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jobs_history_id_seq OWNED BY public.jobs_history.id;


--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jobs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jobs_id_seq OWNER TO postgres;

--
-- Name: jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jobs_id_seq OWNED BY public.jobs.id;


--
-- Name: lending_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lending_items (
    id integer NOT NULL,
    item character varying,
    description character varying,
    leant_to integer,
    date_borrowed date,
    notes character varying,
    _owner_ integer,
    lending_items_categories_id integer
);


ALTER TABLE public.lending_items OWNER TO postgres;

--
-- Name: lending_items_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lending_items_categories (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.lending_items_categories OWNER TO postgres;

--
-- Name: lending_items_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lending_items_categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lending_items_categories_id_seq OWNER TO postgres;

--
-- Name: lending_items_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lending_items_categories_id_seq OWNED BY public.lending_items_categories.id;


--
-- Name: lending_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lending_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lending_items_id_seq OWNER TO postgres;

--
-- Name: lending_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lending_items_id_seq OWNED BY public.lending_items.id;


--
-- Name: lists; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lists (
    id integer NOT NULL,
    name character varying,
    record_name character varying,
    groups_id integer
);


ALTER TABLE public.lists OWNER TO postgres;

--
-- Name: lists_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lists_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lists_id_seq OWNER TO postgres;

--
-- Name: lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lists_id_seq OWNED BY public.lists.id;


--
-- Name: locations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.locations (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.locations OWNER TO postgres;

--
-- Name: locations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.locations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.locations_id_seq OWNER TO postgres;

--
-- Name: locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.locations_id_seq OWNED BY public.locations.id;


--
-- Name: lostfound; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.lostfound (
    id integer NOT NULL,
    what character varying,
    item character varying,
    contact character varying,
    date date
);


ALTER TABLE public.lostfound OWNER TO postgres;

--
-- Name: lostfound_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.lostfound_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lostfound_id_seq OWNER TO postgres;

--
-- Name: lostfound_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.lostfound_id_seq OWNED BY public.lostfound.id;


--
-- Name: mail_lists; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mail_lists (
    id integer NOT NULL,
    name character varying,
    active boolean DEFAULT true,
    send_to character varying,
    allow_from_outside boolean DEFAULT false,
    allow_from_outside_subscribers boolean DEFAULT false,
    announce_only boolean DEFAULT false,
    footer character varying,
    archive boolean DEFAULT true,
    archives_public boolean DEFAULT true,
    description character varying,
    posting_role character varying,
    subscribing_role character varying,
    show_subscribers boolean DEFAULT true,
    password character varying,
    accept_from character varying
);


ALTER TABLE public.mail_lists OWNER TO postgres;

--
-- Name: mail_lists_digest; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mail_lists_digest (
    mail_lists_id integer,
    people_id integer
);


ALTER TABLE public.mail_lists_digest OWNER TO postgres;

--
-- Name: mail_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.mail_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.mail_lists_id_seq OWNER TO postgres;

--
-- Name: mail_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.mail_lists_id_seq OWNED BY public.mail_lists.id;


--
-- Name: mail_lists_people; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.mail_lists_people (
    mail_lists_id integer,
    people_id integer
);


ALTER TABLE public.mail_lists_people OWNER TO postgres;

--
-- Name: main_calendar_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main_calendar_events (
    id integer NOT NULL,
    date date,
    _owner_ integer,
    repeat character varying,
    end_date date,
    end_time time without time zone,
    event character varying,
    start_time time without time zone,
    register_people boolean DEFAULT false,
    main_calendar_events_locations_id integer,
    _timestamp_ timestamp without time zone,
    notes character varying,
    main_calendar_events_categories_id integer,
    event_id integer
);


ALTER TABLE public.main_calendar_events OWNER TO postgres;

--
-- Name: main_calendar_events_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main_calendar_events_categories (
    id integer NOT NULL,
    text character varying,
    color character varying
);


ALTER TABLE public.main_calendar_events_categories OWNER TO postgres;

--
-- Name: main_calendar_events_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.main_calendar_events_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_calendar_events_categories_id_seq OWNER TO postgres;

--
-- Name: main_calendar_events_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.main_calendar_events_categories_id_seq OWNED BY public.main_calendar_events_categories.id;


--
-- Name: main_calendar_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.main_calendar_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_calendar_events_id_seq OWNER TO postgres;

--
-- Name: main_calendar_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.main_calendar_events_id_seq OWNED BY public.main_calendar_events.id;


--
-- Name: main_calendar_events_locations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main_calendar_events_locations (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.main_calendar_events_locations OWNER TO postgres;

--
-- Name: main_calendar_events_locations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.main_calendar_events_locations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_calendar_events_locations_id_seq OWNER TO postgres;

--
-- Name: main_calendar_events_locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.main_calendar_events_locations_id_seq OWNED BY public.main_calendar_events_locations.id;


--
-- Name: main_calendar_events_registrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main_calendar_events_registrations (
    id integer NOT NULL,
    main_calendar_events_id integer,
    person integer,
    note character varying
);


ALTER TABLE public.main_calendar_events_registrations OWNER TO postgres;

--
-- Name: main_calendar_events_registrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.main_calendar_events_registrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_calendar_events_registrations_id_seq OWNER TO postgres;

--
-- Name: main_calendar_events_registrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.main_calendar_events_registrations_id_seq OWNED BY public.main_calendar_events_registrations.id;


--
-- Name: main_calendar_events_reminders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.main_calendar_events_reminders (
    id integer NOT NULL,
    main_calendar_events_id integer,
    note text,
    email character varying,
    _owner_ integer,
    before boolean DEFAULT true,
    num integer,
    unit character varying,
    repeat_days character varying DEFAULT 'each day'::character varying
);


ALTER TABLE public.main_calendar_events_reminders OWNER TO postgres;

--
-- Name: main_calendar_events_reminders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.main_calendar_events_reminders_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_calendar_events_reminders_id_seq OWNER TO postgres;

--
-- Name: main_calendar_events_reminders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.main_calendar_events_reminders_id_seq OWNED BY public.main_calendar_events_reminders.id;


--
-- Name: meal_events; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.meal_events (
    id integer NOT NULL,
    date date,
    additional_cooks integer,
    adult_budget_amount real,
    adult_price real,
    assist integer,
    clean1 integer,
    clean2 integer,
    kid_budget_amount real,
    kid_price real,
    closed boolean DEFAULT false,
    max_people integer,
    menu character varying,
    _owner_ integer,
    setup integer,
    start_time time without time zone,
    invoiced boolean DEFAULT false,
    under_five_budget_amount real,
    under_five_price real,
    notes character varying,
    end_time time without time zone,
    closed_message character varying,
    time_to_automatically_close time without time zone,
    close_days_before_meal integer,
    _timestamp_ timestamp without time zone
);


ALTER TABLE public.meal_events OWNER TO postgres;

--
-- Name: meal_events_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.meal_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meal_events_id_seq OWNER TO postgres;

--
-- Name: meal_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.meal_events_id_seq OWNED BY public.meal_events.id;


--
-- Name: meal_people; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.meal_people (
    id integer NOT NULL,
    meal_events_id integer,
    num_adults integer,
    num_kids integer,
    num_under_fives integer,
    dish character varying,
    households_id integer,
    notes character varying
);


ALTER TABLE public.meal_people OWNER TO postgres;

--
-- Name: meal_people_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.meal_people_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meal_people_id_seq OWNER TO postgres;

--
-- Name: meal_people_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.meal_people_id_seq OWNED BY public.meal_people.id;


--
-- Name: meetings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.meetings (
    id integer NOT NULL,
    title character varying,
    days character varying,
    _owner_ integer
);


ALTER TABLE public.meetings OWNER TO postgres;

--
-- Name: meetings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.meetings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.meetings_id_seq OWNER TO postgres;

--
-- Name: meetings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.meetings_id_seq OWNED BY public.meetings.id;


--
-- Name: meetings_people; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.meetings_people (
    meetings_id integer,
    people_id integer,
    color character varying,
    days character varying
);


ALTER TABLE public.meetings_people OWNER TO postgres;

--
-- Name: menu; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.menu (
    id integer NOT NULL,
    name character varying,
    _order_ integer
);


ALTER TABLE public.menu OWNER TO postgres;

--
-- Name: menu_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.menu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_id_seq OWNER TO postgres;

--
-- Name: menu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.menu_id_seq OWNED BY public.menu.id;


--
-- Name: menu_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.menu_items (
    id integer NOT NULL,
    people_id integer,
    name character varying,
    _order_ integer
);


ALTER TABLE public.menu_items OWNER TO postgres;

--
-- Name: menu_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.menu_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_items_id_seq OWNER TO postgres;

--
-- Name: menu_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.menu_items_id_seq OWNED BY public.menu_items.id;


--
-- Name: minutes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.minutes (
    id integer NOT NULL,
    date date,
    summary character varying,
    text character varying,
    _timestamp_ timestamp without time zone,
    _owner_ integer,
    likes character varying,
    show_on_news_feed boolean DEFAULT true,
    tsvector tsvector,
    groups_id integer,
    show_on_main_minutes_page boolean DEFAULT true
);


ALTER TABLE public.minutes OWNER TO postgres;

--
-- Name: minutes_attachments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.minutes_attachments (
    id integer NOT NULL,
    minutes_id integer,
    filename character varying
);


ALTER TABLE public.minutes_attachments OWNER TO postgres;

--
-- Name: minutes_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.minutes_attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.minutes_attachments_id_seq OWNER TO postgres;

--
-- Name: minutes_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.minutes_attachments_id_seq OWNED BY public.minutes_attachments.id;


--
-- Name: minutes_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.minutes_comments (
    id integer NOT NULL,
    minutes_id integer,
    reply_to integer,
    comment text,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.minutes_comments OWNER TO postgres;

--
-- Name: minutes_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.minutes_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.minutes_comments_id_seq OWNER TO postgres;

--
-- Name: minutes_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.minutes_comments_id_seq OWNED BY public.minutes_comments.id;


--
-- Name: minutes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.minutes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.minutes_id_seq OWNER TO postgres;

--
-- Name: minutes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.minutes_id_seq OWNED BY public.minutes.id;


--
-- Name: movies; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movies (
    id integer NOT NULL,
    title character varying,
    genre character varying,
    format character varying,
    owner character varying,
    on_loan_to character varying,
    comments character varying
);


ALTER TABLE public.movies OWNER TO postgres;

--
-- Name: movies_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movies_id_seq OWNER TO postgres;

--
-- Name: movies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movies_id_seq OWNED BY public.movies.id;


--
-- Name: news; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news (
    id integer NOT NULL,
    provider character varying,
    item_id integer,
    _owner_ integer,
    _timestamp_ timestamp without time zone,
    last_update timestamp without time zone,
    channel integer
);


ALTER TABLE public.news OWNER TO postgres;

--
-- Name: news_feed; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news_feed (
    id integer NOT NULL,
    _owner_ integer,
    text character varying,
    _timestamp_ timestamp without time zone,
    likes character varying,
    show_on_news_feed boolean DEFAULT true
);


ALTER TABLE public.news_feed OWNER TO postgres;

--
-- Name: news_feed_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.news_feed_comments (
    id integer NOT NULL,
    news_feed_id integer,
    reply_to integer,
    comment character varying,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.news_feed_comments OWNER TO postgres;

--
-- Name: news_feed_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_feed_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_feed_comments_id_seq OWNER TO postgres;

--
-- Name: news_feed_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_feed_comments_id_seq OWNED BY public.news_feed_comments.id;


--
-- Name: news_feed_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_feed_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_feed_id_seq OWNER TO postgres;

--
-- Name: news_feed_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_feed_id_seq OWNED BY public.news_feed.id;


--
-- Name: news_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.news_id_seq OWNER TO postgres;

--
-- Name: news_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.news_id_seq OWNED BY public.news.id;


--
-- Name: notes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notes (
    id integer NOT NULL,
    households_id integer,
    title character varying,
    note bytea,
    category character varying,
    public boolean DEFAULT false
);


ALTER TABLE public.notes OWNER TO postgres;

--
-- Name: notes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notes_id_seq OWNER TO postgres;

--
-- Name: notes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notes_id_seq OWNED BY public.notes.id;


--
-- Name: notices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notices (
    id integer NOT NULL,
    category character varying,
    date date,
    description character varying,
    _owner_ integer,
    title character varying,
    likes character varying,
    show_on_news_feed boolean DEFAULT true
);


ALTER TABLE public.notices OWNER TO postgres;

--
-- Name: notices_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notices_comments (
    id integer NOT NULL,
    notices_id integer,
    reply_to integer,
    comment character varying,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.notices_comments OWNER TO postgres;

--
-- Name: notices_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notices_comments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notices_comments_id_seq OWNER TO postgres;

--
-- Name: notices_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notices_comments_id_seq OWNED BY public.notices_comments.id;


--
-- Name: notices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notices_id_seq OWNER TO postgres;

--
-- Name: notices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notices_id_seq OWNED BY public.notices.id;


--
-- Name: notices_pictures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notices_pictures (
    id integer NOT NULL,
    notices_id integer,
    filename character varying
);


ALTER TABLE public.notices_pictures OWNER TO postgres;

--
-- Name: notices_pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.notices_pictures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notices_pictures_id_seq OWNER TO postgres;

--
-- Name: notices_pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.notices_pictures_id_seq OWNED BY public.notices_pictures.id;


--
-- Name: offices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.offices (
    id integer NOT NULL,
    office character varying,
    description character varying,
    notes character varying
);


ALTER TABLE public.offices OWNER TO postgres;

--
-- Name: offices_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.offices_history (
    id integer NOT NULL,
    person integer,
    start_date date,
    end_date date,
    notes character varying,
    offices_id integer
);


ALTER TABLE public.offices_history OWNER TO postgres;

--
-- Name: offices_history_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.offices_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.offices_history_id_seq OWNER TO postgres;

--
-- Name: offices_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.offices_history_id_seq OWNED BY public.offices_history.id;


--
-- Name: offices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.offices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.offices_id_seq OWNER TO postgres;

--
-- Name: offices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.offices_id_seq OWNED BY public.offices.id;


--
-- Name: pages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pages (
    id integer NOT NULL,
    embed boolean DEFAULT false,
    enabled boolean DEFAULT false,
    name character varying,
    text character varying,
    url character varying,
    public boolean DEFAULT false,
    roles character varying
);


ALTER TABLE public.pages OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pages_id_seq OWNER TO postgres;

--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pages_id_seq OWNED BY public.pages.id;


--
-- Name: payees; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payees (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.payees OWNER TO postgres;

--
-- Name: payees_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payees_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payees_id_seq OWNER TO postgres;

--
-- Name: payees_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payees_id_seq OWNED BY public.payees.id;


--
-- Name: people; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.people (
    id integer NOT NULL,
    first character varying,
    last character varying,
    email character varying,
    user_name character varying,
    password character varying,
    active boolean DEFAULT true,
    address character varying,
    birthday date,
    households_id integer,
    middle character varying,
    phone character varying,
    theme character varying,
    resident boolean DEFAULT false,
    last_login timestamp without time zone,
    owner boolean DEFAULT false,
    picture character varying,
    bio character varying,
    show_birthday_on_calendar boolean DEFAULT true,
    data character varying,
    data_keys character varying,
    uuid character varying,
    timezone character varying,
    can_post_to_mail_lists boolean DEFAULT true,
    emergency_contacts character varying,
    pronouns character varying,
    send_email_when_someone_comments_on_my_posts boolean DEFAULT true,
    json jsonb,
    date_added date DEFAULT now(),
    send_email_when_someone_posts_to_the_site boolean DEFAULT false,
    reset_id character varying,
    remove_me_when_i_post_to_lists boolean DEFAULT false,
    hide_email_on_site boolean DEFAULT false,
    must_change_credentials boolean DEFAULT false
);


ALTER TABLE public.people OWNER TO postgres;

--
-- Name: people_bio_pictures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.people_bio_pictures (
    id integer NOT NULL,
    people_id integer,
    filename character varying
);


ALTER TABLE public.people_bio_pictures OWNER TO postgres;

--
-- Name: people_bio_pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.people_bio_pictures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.people_bio_pictures_id_seq OWNER TO postgres;

--
-- Name: people_bio_pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.people_bio_pictures_id_seq OWNED BY public.people_bio_pictures.id;


--
-- Name: people_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.people_groups (
    people_id integer,
    groups_id integer
);


ALTER TABLE public.people_groups OWNER TO postgres;

--
-- Name: people_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.people_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.people_id_seq OWNER TO postgres;

--
-- Name: people_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.people_id_seq OWNED BY public.people.id;


--
-- Name: people_mail_lists; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.people_mail_lists (
    id integer NOT NULL,
    people_id integer,
    name character varying,
    people_ids text
);


ALTER TABLE public.people_mail_lists OWNER TO postgres;

--
-- Name: people_mail_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.people_mail_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.people_mail_lists_id_seq OWNER TO postgres;

--
-- Name: people_mail_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.people_mail_lists_id_seq OWNED BY public.people_mail_lists.id;


--
-- Name: people_skills; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.people_skills (
    id integer NOT NULL,
    people_id integer,
    skill character varying
);


ALTER TABLE public.people_skills OWNER TO postgres;

--
-- Name: people_skills_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.people_skills_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.people_skills_id_seq OWNER TO postgres;

--
-- Name: people_skills_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.people_skills_id_seq OWNED BY public.people_skills.id;


--
-- Name: people_work_tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.people_work_tasks (
    people_id integer,
    work_tasks_id integer,
    date date,
    hours integer
);


ALTER TABLE public.people_work_tasks OWNER TO postgres;

--
-- Name: pets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pets (
    id integer NOT NULL,
    households_id integer,
    name character varying,
    notes character varying,
    photo character varying
);


ALTER TABLE public.pets OWNER TO postgres;

--
-- Name: pets_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pets_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pets_id_seq OWNER TO postgres;

--
-- Name: pets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pets_id_seq OWNED BY public.pets.id;


--
-- Name: pictures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pictures (
    id integer NOT NULL,
    file character varying,
    caption character varying,
    _owner_ integer,
    _timestamp_ timestamp without time zone,
    width integer,
    height integer,
    likes character varying,
    can_show_on_login_page boolean DEFAULT true,
    pictures_galleries_id integer,
    show_on_news_feed boolean DEFAULT true
);


ALTER TABLE public.pictures OWNER TO postgres;

--
-- Name: pictures_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pictures_comments (
    id integer NOT NULL,
    pictures_id integer,
    reply_to integer,
    comment text,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.pictures_comments OWNER TO postgres;

--
-- Name: pictures_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pictures_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pictures_comments_id_seq OWNER TO postgres;

--
-- Name: pictures_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pictures_comments_id_seq OWNED BY public.pictures_comments.id;


--
-- Name: pictures_galleries; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pictures_galleries (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.pictures_galleries OWNER TO postgres;

--
-- Name: pictures_galleries_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pictures_galleries_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pictures_galleries_id_seq OWNER TO postgres;

--
-- Name: pictures_galleries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pictures_galleries_id_seq OWNED BY public.pictures_galleries.id;


--
-- Name: pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pictures_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pictures_id_seq OWNER TO postgres;

--
-- Name: pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pictures_id_seq OWNED BY public.pictures.id;


--
-- Name: pictures_pictures_tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pictures_pictures_tags (
    pictures_id integer,
    pictures_tags_id integer
);


ALTER TABLE public.pictures_pictures_tags OWNER TO postgres;

--
-- Name: pictures_tags; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pictures_tags (
    id integer NOT NULL,
    tag text
);


ALTER TABLE public.pictures_tags OWNER TO postgres;

--
-- Name: pictures_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pictures_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pictures_tags_id_seq OWNER TO postgres;

--
-- Name: pictures_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pictures_tags_id_seq OWNED BY public.pictures_tags.id;


--
-- Name: recipes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipes (
    id integer NOT NULL,
    households_id integer,
    title character varying,
    ingredients text,
    directions text,
    coho boolean,
    _owner_ integer,
    show_on_news_feed boolean DEFAULT true,
    recipes_categories_id integer
);


ALTER TABLE public.recipes OWNER TO postgres;

--
-- Name: recipes_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipes_categories (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.recipes_categories OWNER TO postgres;

--
-- Name: recipes_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipes_categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipes_categories_id_seq OWNER TO postgres;

--
-- Name: recipes_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipes_categories_id_seq OWNED BY public.recipes_categories.id;


--
-- Name: recipes_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipes_comments (
    id integer NOT NULL,
    recipes_id integer,
    reply_to integer,
    comment character varying,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.recipes_comments OWNER TO postgres;

--
-- Name: recipes_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipes_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipes_comments_id_seq OWNER TO postgres;

--
-- Name: recipes_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipes_comments_id_seq OWNED BY public.recipes_comments.id;


--
-- Name: recipes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipes_id_seq OWNER TO postgres;

--
-- Name: recipes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipes_id_seq OWNED BY public.recipes.id;


--
-- Name: recommendations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recommendations (
    id integer NOT NULL,
    recommendation character varying,
    recommended_by character varying,
    comments character varying
);


ALTER TABLE public.recommendations OWNER TO postgres;

--
-- Name: recommendations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recommendations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recommendations_id_seq OWNER TO postgres;

--
-- Name: recommendations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recommendations_id_seq OWNED BY public.recommendations.id;


--
-- Name: reviews; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reviews (
    id integer NOT NULL,
    title character varying,
    review character varying,
    _owner_ integer,
    likes character varying,
    categories_id integer,
    show_on_news_feed boolean DEFAULT true,
    contact character varying,
    reviews_categories_id integer
);


ALTER TABLE public.reviews OWNER TO postgres;

--
-- Name: reviews_attachments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reviews_attachments (
    id integer NOT NULL,
    reviews_id integer,
    filename character varying
);


ALTER TABLE public.reviews_attachments OWNER TO postgres;

--
-- Name: reviews_attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reviews_attachments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_attachments_id_seq OWNER TO postgres;

--
-- Name: reviews_attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reviews_attachments_id_seq OWNED BY public.reviews_attachments.id;


--
-- Name: reviews_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reviews_categories (
    id integer NOT NULL,
    text character varying
);


ALTER TABLE public.reviews_categories OWNER TO postgres;

--
-- Name: reviews_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reviews_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_categories_id_seq OWNER TO postgres;

--
-- Name: reviews_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reviews_categories_id_seq OWNED BY public.reviews_categories.id;


--
-- Name: reviews_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reviews_comments (
    id integer NOT NULL,
    reviews_id integer,
    reply_to integer,
    comment character varying,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.reviews_comments OWNER TO postgres;

--
-- Name: reviews_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reviews_comments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_comments_id_seq OWNER TO postgres;

--
-- Name: reviews_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reviews_comments_id_seq OWNED BY public.reviews_comments.id;


--
-- Name: reviews_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reviews_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.reviews_id_seq OWNER TO postgres;

--
-- Name: reviews_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reviews_id_seq OWNED BY public.reviews.id;


--
-- Name: subscribers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.subscribers (
    id integer NOT NULL,
    mail_lists_id integer,
    email character varying,
    name character varying
);


ALTER TABLE public.subscribers OWNER TO postgres;

--
-- Name: subscribers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.subscribers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subscribers_id_seq OWNER TO postgres;

--
-- Name: subscribers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.subscribers_id_seq OWNED BY public.subscribers.id;


--
-- Name: survey_answers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.survey_answers (
    id integer NOT NULL,
    survey_items_id integer,
    answer character varying,
    _owner_ integer
);


ALTER TABLE public.survey_answers OWNER TO postgres;

--
-- Name: survey_answers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.survey_answers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.survey_answers_id_seq OWNER TO postgres;

--
-- Name: survey_answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.survey_answers_id_seq OWNED BY public.survey_answers.id;


--
-- Name: survey_items; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.survey_items (
    id integer NOT NULL,
    surveys_id integer,
    type character varying,
    type_data character varying,
    text character varying,
    pre_text character varying,
    post_text character varying,
    required boolean DEFAULT false,
    _order_ integer
);


ALTER TABLE public.survey_items OWNER TO postgres;

--
-- Name: survey_items_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.survey_items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.survey_items_id_seq OWNER TO postgres;

--
-- Name: survey_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.survey_items_id_seq OWNED BY public.survey_items.id;


--
-- Name: survey_questions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.survey_questions (
    id integer NOT NULL,
    surveys_id integer,
    question text,
    answer_type character varying,
    type_data character varying,
    required boolean,
    pre_text text,
    post_text text,
    _order_ integer,
    _timestamp_ timestamp without time zone
);


ALTER TABLE public.survey_questions OWNER TO postgres;

--
-- Name: survey_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.survey_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.survey_questions_id_seq OWNER TO postgres;

--
-- Name: survey_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.survey_questions_id_seq OWNED BY public.survey_questions.id;


--
-- Name: surveys; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.surveys (
    id integer NOT NULL,
    title character varying,
    description text,
    number_questions boolean,
    show_answers boolean,
    show_names boolean,
    _owner_ integer,
    closed boolean DEFAULT false,
    close_date date
);


ALTER TABLE public.surveys OWNER TO postgres;

--
-- Name: surveys_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.surveys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.surveys_id_seq OWNER TO postgres;

--
-- Name: surveys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.surveys_id_seq OWNED BY public.surveys.id;


--
-- Name: tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tasks (
    id integer NOT NULL,
    description character varying,
    notes character varying,
    num_people_needed character varying,
    workers character varying,
    _owner_ integer,
    groups_id integer
);


ALTER TABLE public.tasks OWNER TO postgres;

--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasks_id_seq OWNER TO postgres;

--
-- Name: tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tasks_id_seq OWNED BY public.tasks.id;


--
-- Name: timebank; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.timebank (
    id integer NOT NULL,
    date date,
    hours real,
    _owner_ integer,
    task character varying,
    recipient integer
);


ALTER TABLE public.timebank OWNER TO postgres;

--
-- Name: timebank_comments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.timebank_comments (
    id integer NOT NULL,
    timebank_id integer,
    reply_to integer,
    comment character varying,
    _timestamp_ timestamp without time zone,
    _owner_ integer
);


ALTER TABLE public.timebank_comments OWNER TO postgres;

--
-- Name: timebank_comments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.timebank_comments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.timebank_comments_id_seq OWNER TO postgres;

--
-- Name: timebank_comments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.timebank_comments_id_seq OWNED BY public.timebank_comments.id;


--
-- Name: timebank_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.timebank_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.timebank_id_seq OWNER TO postgres;

--
-- Name: timebank_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.timebank_id_seq OWNED BY public.timebank.id;


--
-- Name: timebank_requests; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.timebank_requests (
    id integer NOT NULL,
    description character varying,
    _owner_ integer
);


ALTER TABLE public.timebank_requests OWNER TO postgres;

--
-- Name: timebank_requests_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.timebank_requests_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.timebank_requests_id_seq OWNER TO postgres;

--
-- Name: timebank_requests_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.timebank_requests_id_seq OWNED BY public.timebank_requests.id;


--
-- Name: todos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.todos (
    id integer NOT NULL,
    households_id integer,
    task character varying,
    notes text,
    assigned_to integer,
    project character varying
);


ALTER TABLE public.todos OWNER TO postgres;

--
-- Name: todos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.todos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.todos_id_seq OWNER TO postgres;

--
-- Name: todos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.todos_id_seq OWNED BY public.todos.id;


--
-- Name: transactions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transactions (
    id integer NOT NULL,
    accounts_id integer,
    amount double precision,
    date date,
    description character varying,
    households_id integer,
    num character varying,
    reconciled boolean DEFAULT false,
    type character varying,
    transactions_id integer,
    bank_accounts_id integer,
    payees_id integer,
    notes character varying
);


ALTER TABLE public.transactions OWNER TO postgres;

--
-- Name: transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transactions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transactions_id_seq OWNER TO postgres;

--
-- Name: transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.transactions_id_seq OWNED BY public.transactions.id;


--
-- Name: units; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.units (
    id integer NOT NULL,
    unit character varying,
    hoa_dues real
);


ALTER TABLE public.units OWNER TO postgres;

--
-- Name: units_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.units_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.units_id_seq OWNER TO postgres;

--
-- Name: units_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.units_id_seq OWNED BY public.units.id;


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_roles (
    id integer NOT NULL,
    people_id integer,
    user_name character varying,
    role character varying
);


ALTER TABLE public.user_roles OWNER TO postgres;

--
-- Name: user_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_roles_id_seq OWNER TO postgres;

--
-- Name: user_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_roles_id_seq OWNED BY public.user_roles.id;


--
-- Name: vehicles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vehicles (
    id integer NOT NULL,
    households_id integer,
    available_for_sharing boolean DEFAULT false,
    description character varying,
    license_plate character varying,
    notes character varying,
    parking_spot character varying,
    photo character varying
);


ALTER TABLE public.vehicles OWNER TO postgres;

--
-- Name: vehicles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.vehicles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vehicles_id_seq OWNER TO postgres;

--
-- Name: vehicles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.vehicles_id_seq OWNED BY public.vehicles.id;


--
-- Name: votes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.votes (
    id integer NOT NULL,
    decisions_id integer,
    vote character varying,
    _owner_ integer,
    _timestamp_ timestamp without time zone
);


ALTER TABLE public.votes OWNER TO postgres;

--
-- Name: votes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.votes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.votes_id_seq OWNER TO postgres;

--
-- Name: votes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.votes_id_seq OWNED BY public.votes.id;


--
-- Name: work_hours; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.work_hours (
    id integer NOT NULL,
    work_tasks_id integer,
    date date,
    hours double precision,
    _owner_ integer,
    notes character varying,
    link integer
);


ALTER TABLE public.work_hours OWNER TO postgres;

--
-- Name: work_hours_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.work_hours_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.work_hours_id_seq OWNER TO postgres;

--
-- Name: work_hours_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.work_hours_id_seq OWNED BY public.work_hours.id;


--
-- Name: work_tasks; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.work_tasks (
    id integer NOT NULL,
    task character varying,
    active boolean DEFAULT true,
    groups_id integer
);


ALTER TABLE public.work_tasks OWNER TO postgres;

--
-- Name: work_tasks_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.work_tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.work_tasks_id_seq OWNER TO postgres;

--
-- Name: work_tasks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.work_tasks_id_seq OWNED BY public.work_tasks.id;


--
-- Name: _columns_ id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._columns_ ALTER COLUMN id SET DEFAULT nextval('public._columns__id_seq'::regclass);


--
-- Name: _modules_ id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._modules_ ALTER COLUMN id SET DEFAULT nextval('public._modules__id_seq'::regclass);


--
-- Name: _relationship_defs_ id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._relationship_defs_ ALTER COLUMN id SET DEFAULT nextval('public._relationship_defs__id_seq'::regclass);


--
-- Name: _roles_ id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._roles_ ALTER COLUMN id SET DEFAULT nextval('public._roles__id_seq'::regclass);


--
-- Name: _settings_ id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._settings_ ALTER COLUMN id SET DEFAULT nextval('public._settings__id_seq'::regclass);


--
-- Name: _tables_ id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._tables_ ALTER COLUMN id SET DEFAULT nextval('public._tables__id_seq'::regclass);


--
-- Name: _views_ id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._views_ ALTER COLUMN id SET DEFAULT nextval('public._views__id_seq'::regclass);


--
-- Name: accounts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts ALTER COLUMN id SET DEFAULT nextval('public.accounts_id_seq'::regclass);


--
-- Name: accounts_budgets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts_budgets ALTER COLUMN id SET DEFAULT nextval('public.accounts_budgets_id_seq'::regclass);


--
-- Name: additional_emails id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_emails ALTER COLUMN id SET DEFAULT nextval('public.additional_emails_id_seq'::regclass);


--
-- Name: additional_senders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_senders ALTER COLUMN id SET DEFAULT nextval('public.additional_senders_id_seq'::regclass);


--
-- Name: automatic_transactions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automatic_transactions ALTER COLUMN id SET DEFAULT nextval('public.automatic_transactions_id_seq'::regclass);


--
-- Name: bank_accounts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_accounts ALTER COLUMN id SET DEFAULT nextval('public.bank_accounts_id_seq'::regclass);


--
-- Name: bank_accounts_balances id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_accounts_balances ALTER COLUMN id SET DEFAULT nextval('public.bank_accounts_balances_id_seq'::regclass);


--
-- Name: bicycles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bicycles ALTER COLUMN id SET DEFAULT nextval('public.bicycles_id_seq'::regclass);


--
-- Name: birthday_reminders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.birthday_reminders ALTER COLUMN id SET DEFAULT nextval('public.birthday_reminders_id_seq'::regclass);


--
-- Name: birthdays_events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.birthdays_events ALTER COLUMN id SET DEFAULT nextval('public.birthdays_events_id_seq'::regclass);


--
-- Name: blackboard id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blackboard ALTER COLUMN id SET DEFAULT nextval('public.blackboard_id_seq'::regclass);


--
-- Name: budgets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.budgets ALTER COLUMN id SET DEFAULT nextval('public.budgets_id_seq'::regclass);


--
-- Name: budgets_amounts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.budgets_amounts ALTER COLUMN id SET DEFAULT nextval('public.budgets_amounts_id_seq'::regclass);


--
-- Name: calendar_guest_room_events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendar_guest_room_events ALTER COLUMN id SET DEFAULT nextval('public.calendar_guest_room_events_id_seq'::regclass);


--
-- Name: contacts id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contacts ALTER COLUMN id SET DEFAULT nextval('public.contacts_id_seq'::regclass);


--
-- Name: decisions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions ALTER COLUMN id SET DEFAULT nextval('public.decisions_id_seq'::regclass);


--
-- Name: decisions_agree id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_agree ALTER COLUMN id SET DEFAULT nextval('public.decisions_agree_id_seq'::regclass);


--
-- Name: decisions_attachments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_attachments ALTER COLUMN id SET DEFAULT nextval('public.decisions_attachments_id_seq'::regclass);


--
-- Name: decisions_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_comments ALTER COLUMN id SET DEFAULT nextval('public.decisions_comments_id_seq'::regclass);


--
-- Name: decisions_rank id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_rank ALTER COLUMN id SET DEFAULT nextval('public.decisions_rank_id_seq'::regclass);


--
-- Name: discussions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions ALTER COLUMN id SET DEFAULT nextval('public.discussions_id_seq'::regclass);


--
-- Name: discussions_attachments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_attachments ALTER COLUMN id SET DEFAULT nextval('public.discussions_attachments_id_seq'::regclass);


--
-- Name: discussions_categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_categories ALTER COLUMN id SET DEFAULT nextval('public.discussions_categories_id_seq'::regclass);


--
-- Name: discussions_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_comments ALTER COLUMN id SET DEFAULT nextval('public.discussions_comments_id_seq'::regclass);


--
-- Name: documents id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents ALTER COLUMN id SET DEFAULT nextval('public.documents_id_seq'::regclass);


--
-- Name: documents_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents_comments ALTER COLUMN id SET DEFAULT nextval('public.documents_comments_id_seq'::regclass);


--
-- Name: documents_types id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents_types ALTER COLUMN id SET DEFAULT nextval('public.documents_types_id_seq'::regclass);


--
-- Name: equipment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipment ALTER COLUMN id SET DEFAULT nextval('public.equipment_id_seq'::regclass);


--
-- Name: general_meetings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.general_meetings ALTER COLUMN id SET DEFAULT nextval('public.general_meetings_id_seq'::regclass);


--
-- Name: group_links id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_links ALTER COLUMN id SET DEFAULT nextval('public.group_links_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);


--
-- Name: groups_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups_jobs ALTER COLUMN id SET DEFAULT nextval('public.groups_jobs_id_seq'::regclass);


--
-- Name: guest_room_events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.guest_room_events ALTER COLUMN id SET DEFAULT nextval('public.guest_room_events_id_seq'::regclass);


--
-- Name: homes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.homes ALTER COLUMN id SET DEFAULT nextval('public.homes_id_seq'::regclass);


--
-- Name: jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs ALTER COLUMN id SET DEFAULT nextval('public.jobs_id_seq'::regclass);


--
-- Name: jobs_history id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs_history ALTER COLUMN id SET DEFAULT nextval('public.jobs_history_id_seq'::regclass);


--
-- Name: lending_items id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lending_items ALTER COLUMN id SET DEFAULT nextval('public.lending_items_id_seq'::regclass);


--
-- Name: lending_items_categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lending_items_categories ALTER COLUMN id SET DEFAULT nextval('public.lending_items_categories_id_seq'::regclass);


--
-- Name: lists id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists ALTER COLUMN id SET DEFAULT nextval('public.lists_id_seq'::regclass);


--
-- Name: locations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.locations ALTER COLUMN id SET DEFAULT nextval('public.locations_id_seq'::regclass);


--
-- Name: lostfound id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lostfound ALTER COLUMN id SET DEFAULT nextval('public.lostfound_id_seq'::regclass);


--
-- Name: mail_lists id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail_lists ALTER COLUMN id SET DEFAULT nextval('public.mail_lists_id_seq'::regclass);


--
-- Name: main_calendar_events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events ALTER COLUMN id SET DEFAULT nextval('public.main_calendar_events_id_seq'::regclass);


--
-- Name: main_calendar_events_categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_categories ALTER COLUMN id SET DEFAULT nextval('public.main_calendar_events_categories_id_seq'::regclass);


--
-- Name: main_calendar_events_locations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_locations ALTER COLUMN id SET DEFAULT nextval('public.main_calendar_events_locations_id_seq'::regclass);


--
-- Name: main_calendar_events_registrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_registrations ALTER COLUMN id SET DEFAULT nextval('public.main_calendar_events_registrations_id_seq'::regclass);


--
-- Name: main_calendar_events_reminders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_reminders ALTER COLUMN id SET DEFAULT nextval('public.main_calendar_events_reminders_id_seq'::regclass);


--
-- Name: meal_events id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meal_events ALTER COLUMN id SET DEFAULT nextval('public.meal_events_id_seq'::regclass);


--
-- Name: meal_people id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meal_people ALTER COLUMN id SET DEFAULT nextval('public.meal_people_id_seq'::regclass);


--
-- Name: meetings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meetings ALTER COLUMN id SET DEFAULT nextval('public.meetings_id_seq'::regclass);


--
-- Name: menu id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu ALTER COLUMN id SET DEFAULT nextval('public.menu_id_seq'::regclass);


--
-- Name: menu_items id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items ALTER COLUMN id SET DEFAULT nextval('public.menu_items_id_seq'::regclass);


--
-- Name: minutes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes ALTER COLUMN id SET DEFAULT nextval('public.minutes_id_seq'::regclass);


--
-- Name: minutes_attachments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes_attachments ALTER COLUMN id SET DEFAULT nextval('public.minutes_attachments_id_seq'::regclass);


--
-- Name: minutes_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes_comments ALTER COLUMN id SET DEFAULT nextval('public.minutes_comments_id_seq'::regclass);


--
-- Name: movies id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies ALTER COLUMN id SET DEFAULT nextval('public.movies_id_seq'::regclass);


--
-- Name: news id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news ALTER COLUMN id SET DEFAULT nextval('public.news_id_seq'::regclass);


--
-- Name: news_feed id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_feed ALTER COLUMN id SET DEFAULT nextval('public.news_feed_id_seq'::regclass);


--
-- Name: news_feed_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_feed_comments ALTER COLUMN id SET DEFAULT nextval('public.news_feed_comments_id_seq'::regclass);


--
-- Name: notes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notes ALTER COLUMN id SET DEFAULT nextval('public.notes_id_seq'::regclass);


--
-- Name: notices id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices ALTER COLUMN id SET DEFAULT nextval('public.notices_id_seq'::regclass);


--
-- Name: notices_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices_comments ALTER COLUMN id SET DEFAULT nextval('public.notices_comments_id_seq'::regclass);


--
-- Name: notices_pictures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices_pictures ALTER COLUMN id SET DEFAULT nextval('public.notices_pictures_id_seq'::regclass);


--
-- Name: offices id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices ALTER COLUMN id SET DEFAULT nextval('public.offices_id_seq'::regclass);


--
-- Name: offices_history id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices_history ALTER COLUMN id SET DEFAULT nextval('public.offices_history_id_seq'::regclass);


--
-- Name: pages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pages ALTER COLUMN id SET DEFAULT nextval('public.pages_id_seq'::regclass);


--
-- Name: payees id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payees ALTER COLUMN id SET DEFAULT nextval('public.payees_id_seq'::regclass);


--
-- Name: people id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people ALTER COLUMN id SET DEFAULT nextval('public.people_id_seq'::regclass);


--
-- Name: people_bio_pictures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_bio_pictures ALTER COLUMN id SET DEFAULT nextval('public.people_bio_pictures_id_seq'::regclass);


--
-- Name: people_mail_lists id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_mail_lists ALTER COLUMN id SET DEFAULT nextval('public.people_mail_lists_id_seq'::regclass);


--
-- Name: people_skills id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_skills ALTER COLUMN id SET DEFAULT nextval('public.people_skills_id_seq'::regclass);


--
-- Name: pets id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pets ALTER COLUMN id SET DEFAULT nextval('public.pets_id_seq'::regclass);


--
-- Name: pictures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures ALTER COLUMN id SET DEFAULT nextval('public.pictures_id_seq'::regclass);


--
-- Name: pictures_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_comments ALTER COLUMN id SET DEFAULT nextval('public.pictures_comments_id_seq'::regclass);


--
-- Name: pictures_galleries id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_galleries ALTER COLUMN id SET DEFAULT nextval('public.pictures_galleries_id_seq'::regclass);


--
-- Name: pictures_tags id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_tags ALTER COLUMN id SET DEFAULT nextval('public.pictures_tags_id_seq'::regclass);


--
-- Name: recipes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes ALTER COLUMN id SET DEFAULT nextval('public.recipes_id_seq'::regclass);


--
-- Name: recipes_categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes_categories ALTER COLUMN id SET DEFAULT nextval('public.recipes_categories_id_seq'::regclass);


--
-- Name: recipes_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes_comments ALTER COLUMN id SET DEFAULT nextval('public.recipes_comments_id_seq'::regclass);


--
-- Name: recommendations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recommendations ALTER COLUMN id SET DEFAULT nextval('public.recommendations_id_seq'::regclass);


--
-- Name: reviews id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews ALTER COLUMN id SET DEFAULT nextval('public.reviews_id_seq'::regclass);


--
-- Name: reviews_attachments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_attachments ALTER COLUMN id SET DEFAULT nextval('public.reviews_attachments_id_seq'::regclass);


--
-- Name: reviews_categories id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_categories ALTER COLUMN id SET DEFAULT nextval('public.reviews_categories_id_seq'::regclass);


--
-- Name: reviews_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_comments ALTER COLUMN id SET DEFAULT nextval('public.reviews_comments_id_seq'::regclass);


--
-- Name: subscribers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscribers ALTER COLUMN id SET DEFAULT nextval('public.subscribers_id_seq'::regclass);


--
-- Name: survey_answers id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_answers ALTER COLUMN id SET DEFAULT nextval('public.survey_answers_id_seq'::regclass);


--
-- Name: survey_items id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_items ALTER COLUMN id SET DEFAULT nextval('public.survey_items_id_seq'::regclass);


--
-- Name: survey_questions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_questions ALTER COLUMN id SET DEFAULT nextval('public.survey_questions_id_seq'::regclass);


--
-- Name: surveys id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.surveys ALTER COLUMN id SET DEFAULT nextval('public.surveys_id_seq'::regclass);


--
-- Name: tasks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks ALTER COLUMN id SET DEFAULT nextval('public.tasks_id_seq'::regclass);


--
-- Name: timebank id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank ALTER COLUMN id SET DEFAULT nextval('public.timebank_id_seq'::regclass);


--
-- Name: timebank_comments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank_comments ALTER COLUMN id SET DEFAULT nextval('public.timebank_comments_id_seq'::regclass);


--
-- Name: timebank_requests id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank_requests ALTER COLUMN id SET DEFAULT nextval('public.timebank_requests_id_seq'::regclass);


--
-- Name: todos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.todos ALTER COLUMN id SET DEFAULT nextval('public.todos_id_seq'::regclass);


--
-- Name: transactions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions ALTER COLUMN id SET DEFAULT nextval('public.transactions_id_seq'::regclass);


--
-- Name: units id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.units ALTER COLUMN id SET DEFAULT nextval('public.units_id_seq'::regclass);


--
-- Name: user_roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles ALTER COLUMN id SET DEFAULT nextval('public.user_roles_id_seq'::regclass);


--
-- Name: vehicles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehicles ALTER COLUMN id SET DEFAULT nextval('public.vehicles_id_seq'::regclass);


--
-- Name: votes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.votes ALTER COLUMN id SET DEFAULT nextval('public.votes_id_seq'::regclass);


--
-- Name: work_hours id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work_hours ALTER COLUMN id SET DEFAULT nextval('public.work_hours_id_seq'::regclass);


--
-- Name: work_tasks id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work_tasks ALTER COLUMN id SET DEFAULT nextval('public.work_tasks_id_seq'::regclass);


--
-- Data for Name: _columns_; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public._columns_ (id, _views__id, name, display_name, display_length, is_hidden, is_read_only, is_required, type, type_args, is_unique, post_text, pre_text, show_previous_values_dropdown, title, json, cell_style, column_head, align_right) FROM stdin;
\.


--
-- Data for Name: _modules_; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public._modules_ (id, class, name, json) FROM stdin;
1	mosaic.LoginPage	LoginPage	{"m_show_today": true, "m_show_banner": true, "m_show_pictures": true, "m_show_upcoming": true, "m_show_blackboard": false}
2	email.MailLists	MailLists	{"m_active": true, "m_read_and_send": true, "m_insert_headers": true, "m_store_protocol": "imaps", "m_add_sender_to_from": false, "m_add_list_name_to_subject": false}
7	accounting.Accounting	Accounting	{"m_label": "HOA", "m_active": true, "m_is_secured": false, "m_display_name": "Accounting", "m_hoa_account_id": 0, "m_statement_date": 27, "m_statement_from": null, "m_send_statements": false, "m_statement_intro": "Hello,<br />  this automatically generated email is your monthly HOA account statement.", "m_arrears_fee_date": 0, "m_hoa_invoice_date": 26, "m_arrears_account_id": 0, "m_balance_sheet_title": "Balance Sheet", "m_charge_arrears_fees": false, "m_create_hoa_invoices": false, "m_default_bank_account": 0, "m_income_statement_title": "Income Statement", "m_statement_zero_balance": "Since your balance is zero, thank you for being current on your account.", "m_default_interest_account": 0, "m_support_email_statements": true, "m_statement_negative_balance": "Since your balance is negative you have a credit and therefore don't need to pay at this time.", "m_statement_positive_balance": "Please pay off your balance as soon as possible."}
5	meals.MealEventProvider	Common Meals	{"m_ages": ["adult", "kid", "under_five"], "m_jobs": ["additional_cooks", "assist", "setup", "clean1", "clean2"], "m_role": null, "m_uuid": "da76bdaa-acd7-4b52-8d76-259776b23d0c", "m_color": "#7FDBFF", "m_rates": null, "m_active": true, "m_extras": [0.4, 0.2, 0.0], "m_styles": null, "m_budgets": [4.0, 2.0, 0.0], "m_agreements": null, "m_cook_label": "cook", "m_ical_items": ["event", "notes", "posted by"], "m_is_secured": true, "m_print_text": null, "m_resolution": 5, "m_event_label": "event", "m_display_name": "Common Meals", "m_events_table": "meal_events", "m_minimum_rate": 0.0, "m_show_on_menu": true, "m_charge_hourly": false, "m_default_notes": null, "m_display_items": ["menu", "cook", "jobs", "open/closed"], "m_display_names": ["adults (13+)", "kids (5-12)", "under_fives"], "m_other_columns": null, "m_tooltip_items": null, "m_charge_for_use": false, "m_check_overlaps": true, "m_end_date_label": "end date", "m_option_columns": null, "m_rate_post_text": null, "m_fee_description": "Event Fee", "m_invoice_nightly": true, "m_locations_table": null, "m_rate_categories": null, "m_text_after_form": null, "m_use_title_field": false, "m_automatic_signup": false, "m_daily_resolution": 30, "m_default_end_time": null, "m_max_people_label": "max people", "m_show_on_upcoming": true, "m_start_date_label": "date", "m_take_aways_label": "take aways", "m_use_closed_field": true, "m_agreements_header": null, "m_credit_account_id": 0, "m_events_can_repeat": false, "m_events_have_color": false, "m_events_have_event": false, "m_household_or_home": "household", "m_location_required": false, "m_support_reminders": false, "m_default_start_time": null, "m_invoice_account_id": 1, "m_signup_individuals": false, "m_support_take_aways": false, "m_display_item_labels": true, "m_support_attachments": false, "m_text_above_calendar": null, "m_tooltip_item_labels": true, "m_calendar_location_id": 0, "m_event_text_show_jobs": true, "m_events_have_category": false, "m_events_have_location": false, "m_send_announcement_to": null, "m_show_balance_message": false, "m_support_waiting_list": false, "m_use_max_people_field": true, "m_waiting_list_message": "There is another event on the calendar that overlaps with this one. Your event has been added and for the days that overlap you will be on a waiting list.", "m_workers_are_families": false, "m_restrict_to_household": false, "m_show_jobs_on_potlucks": true, "m_support_registrations": false, "m_events_have_start_time": true, "m_reminder_subject_items": ["location", "date"], "m_ignore_ages_for_max_count": null, "m_start_with_blank_location": false, "m_support_multiple_locations": false, "m_reservation_report_by_quarter": false, "m_support_announcing_new_events": false, "m_default_close_days_before_meal": 0, "m_num_days_after_meal_to_invoice": 2, "m_only_admins_can_edit_locations": false, "m_only_admins_can_edit_categories": false, "m_allow_signup_of_other_households": false, "m_default_time_to_automatically_close": null}
8	mosaic.calendar.GuestRoomEventProvider	calendar Guest Room	{"m_role": null, "m_uuid": "78eedbd2-486b-4bdb-8966-52ab81845648", "m_color": "#2ECC40", "m_rates": null, "m_active": true, "m_styles": null, "m_agreements": null, "m_ical_items": ["event", "notes", "posted by"], "m_is_secured": true, "m_resolution": 5, "m_event_label": "event", "m_default_view": null, "m_display_name": "Guest Room", "m_events_table": "calendar_guest_room_events", "m_minimum_rate": 0.0, "m_show_on_menu": true, "m_charge_hourly": false, "m_default_notes": null, "m_display_items": ["location", "time", "notes", "posted by"], "m_tooltip_items": null, "m_charge_for_use": false, "m_check_overlaps": true, "m_end_date_label": "last night", "m_rate_post_text": null, "m_fee_description": "Guest Room Fee", "m_locations_table": null, "m_nights_per_year": 0, "m_rate_categories": null, "m_text_after_form": null, "m_use_guest_field": false, "m_daily_resolution": 30, "m_default_end_time": null, "m_show_event_owner": true, "m_show_on_upcoming": true, "m_start_date_label": "first night", "m_agreements_header": null, "m_events_can_repeat": true, "m_events_have_color": false, "m_events_have_event": true, "m_household_or_home": "household", "m_location_required": false, "m_reserved_by_label": "reserved by", "m_support_reminders": false, "m_default_start_time": null, "m_invoice_account_id": 0, "m_display_item_labels": false, "m_support_attachments": false, "m_support_high_season": false, "m_text_above_calendar": null, "m_tooltip_item_labels": true, "m_calendar_location_id": 0, "m_events_have_category": false, "m_events_have_location": false, "m_send_announcement_to": null, "m_support_waiting_list": false, "m_waiting_list_message": "There is another reservation that overlaps with this one. Your reservation has been added and for the nights that overlap you will be on a waiting list.", "m_support_registrations": false, "m_events_have_start_time": false, "m_max_reservation_length": 0, "m_reminder_subject_items": ["location", "date"], "m_start_with_blank_location": false, "m_support_multiple_locations": false, "m_reservation_report_by_quarter": false, "m_support_announcing_new_events": false, "m_allow_multiple_rate_categories": false, "m_only_admins_can_edit_locations": false, "m_only_admins_can_edit_categories": false}
\.


--
-- Data for Name: _relationship_defs_; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public._relationship_defs_ (id, _views__id, many_view_def_name, many_table_column) FROM stdin;
\.


--
-- Data for Name: _roles_; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public._roles_ (id, role) FROM stdin;
\.


--
-- Data for Name: _settings_; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public._settings_ (id, name, value) FROM stdin;
38	show send email to community members link	yes
\.


--
-- Data for Name: _tables_; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public._tables_ (id, name, json) FROM stdin;
\.


--
-- Data for Name: _views_; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public._views_ (id, name, allow_quick_edit, column_names_form, column_names_form_table, column_names_table, default_order_by, "from", record_name, row_window_size, add_button_text, show_num_records, filters, base_filter, column_names_all, show_table_column_picker, delete_button_text, replace_on_quick_edit, after_form, column_heads_can_wrap, edit_button_text) FROM stdin;
\.


--
-- Data for Name: accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.accounts (id, name, type, email, initial_balance, ongoing_account, active) FROM stdin;
1	Common Meals Income	4	\N	\N	\N	t
2	Common Meals Expense	5	\N	\N	\N	t
\.


--
-- Data for Name: accounts_budgets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.accounts_budgets (id, accounts_id, year, amount) FROM stdin;
\.


--
-- Data for Name: additional_emails; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.additional_emails (id, people_id, email) FROM stdin;
\.


--
-- Data for Name: additional_senders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.additional_senders (id, name, email) FROM stdin;
\.


--
-- Data for Name: automatic_transactions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.automatic_transactions (id, accounts_id, amount, bank_accounts_id, description, households_id, generate_day, transaction_day, type, payees_id) FROM stdin;
\.


--
-- Data for Name: bank_accounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bank_accounts (id, name, starting_balance, reconcile) FROM stdin;
\.


--
-- Data for Name: bank_accounts_balances; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bank_accounts_balances (id, amount, bank_accounts_id, date) FROM stdin;
\.


--
-- Data for Name: bicycles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bicycles (id, households_id, available_for_sharing, description, notes, parking_spot, photo) FROM stdin;
\.


--
-- Data for Name: birthday_reminders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.birthday_reminders (id, before, email, note, num, _owner_, repeat_days, unit) FROM stdin;
\.


--
-- Data for Name: birthdays_events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.birthdays_events (id, date, event) FROM stdin;
\.


--
-- Data for Name: blackboard; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.blackboard (id, text, _timestamp_) FROM stdin;
\.


--
-- Data for Name: budgets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.budgets (id, account, type, email, initial_balance) FROM stdin;
\.


--
-- Data for Name: budgets_amounts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.budgets_amounts (id, budgets_id, year, amount) FROM stdin;
\.


--
-- Data for Name: calendar_guest_room_events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.calendar_guest_room_events (id, date, notes, _owner_, repeat, end_date, event_id, event, _timestamp_) FROM stdin;
\.


--
-- Data for Name: contacts; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.contacts (id, households_id, first, last, relationship, phone, email, address, notes, public, category, groups_id) FROM stdin;
\.


--
-- Data for Name: decisions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.decisions (id, date, title, text, status, type, _owner_, _timestamp_, groups_id, tsvector, likes, show_on_news_feed) FROM stdin;
\.


--
-- Data for Name: decisions_agree; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.decisions_agree (id, decisions_id, people_id) FROM stdin;
\.


--
-- Data for Name: decisions_attachments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.decisions_attachments (id, decisions_id, filename) FROM stdin;
\.


--
-- Data for Name: decisions_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.decisions_comments (id, decisions_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: decisions_rank; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.decisions_rank (id, decisions_id, people_id, _order_) FROM stdin;
\.


--
-- Data for Name: discussions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.discussions (id, title, text, _timestamp_, _owner_, likes, show_on_news_feed, discussions_categories_id, close_date) FROM stdin;
\.


--
-- Data for Name: discussions_attachments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.discussions_attachments (id, discussions_id, filename) FROM stdin;
\.


--
-- Data for Name: discussions_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.discussions_categories (id, text) FROM stdin;
\.


--
-- Data for Name: discussions_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.discussions_comments (id, discussions_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documents (id, filename, title, type, groups_id, show_on_main_docs_page, _timestamp_, show_on_news_feed, tsvector, _owner_, folder, kind) FROM stdin;
\.


--
-- Data for Name: documents_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documents_comments (id, documents_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: documents_types; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.documents_types (id, text) FROM stdin;
1	Policy
2	Reference
3	Resource
4	Budget
5	Legal Document
6	Contract
7	Research
8	Report
9	Tip
10	Process
11	Archive
12	Form
\.


--
-- Data for Name: equipment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.equipment (id, item, location, notes) FROM stdin;
\.


--
-- Data for Name: general_meetings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.general_meetings (id, agenda, date, end_time, start_time, facilitator, annual_meeting, budget_meeting) FROM stdin;
\.


--
-- Data for Name: group_links; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.group_links (id, groups_id, title, url, _timestamp_) FROM stdin;
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.groups (id, name, mandate, calendar, self_joining, active, calendar_public, role, calendar_access_policy, private, items) FROM stdin;
\.


--
-- Data for Name: groups_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.groups_jobs (id, groups_id, people_id, job, start, "end") FROM stdin;
\.


--
-- Data for Name: guest_room_events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.guest_room_events (id, date, _owner_, repeat, end_date, event, _timestamp_, notes, event_id) FROM stdin;
\.


--
-- Data for Name: homes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.homes (id, number, units_id, pays_hoa, pays_water) FROM stdin;
\.


--
-- Data for Name: households; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.households (id, name, contact, cm_account_active, meal_work_exempt, homes_id, active, bio, emergency_info) FROM stdin;
\.


--
-- Data for Name: households_events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.households_events (id, date, notes, _owner_, repeat, end_date, event_id, event, start_time, end_time, _timestamp_, households_id) FROM stdin;
\.


--
-- Data for Name: households_events_reminders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.households_events_reminders (id, households_events_id, before, email, note, num, _owner_, repeat_days, unit) FROM stdin;
\.


--
-- Data for Name: households_homes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.households_homes (households_id, homes_id) FROM stdin;
\.


--
-- Data for Name: households_pictures; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.households_pictures (id, households_id, filename) FROM stdin;
\.


--
-- Data for Name: jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jobs (id, job, description, notes, term_type, groups_id, num_people_needed) FROM stdin;
\.


--
-- Data for Name: jobs_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jobs_history (id, person, start, "end", jobs_id, notes, start_date, end_date) FROM stdin;
\.


--
-- Data for Name: lending_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lending_items (id, item, description, leant_to, date_borrowed, notes, _owner_, lending_items_categories_id) FROM stdin;
\.


--
-- Data for Name: lending_items_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lending_items_categories (id, text) FROM stdin;
\.


--
-- Data for Name: lists; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lists (id, name, record_name, groups_id) FROM stdin;
\.


--
-- Data for Name: locations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.locations (id, text) FROM stdin;
\.


--
-- Data for Name: lostfound; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.lostfound (id, what, item, contact, date) FROM stdin;
\.


--
-- Data for Name: mail_lists; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mail_lists (id, name, active, send_to, allow_from_outside, allow_from_outside_subscribers, announce_only, footer, archive, archives_public, description, posting_role, subscribing_role, show_subscribers, password, accept_from) FROM stdin;
\.


--
-- Data for Name: mail_lists_digest; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mail_lists_digest (mail_lists_id, people_id) FROM stdin;
\.


--
-- Data for Name: mail_lists_people; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.mail_lists_people (mail_lists_id, people_id) FROM stdin;
\.


--
-- Data for Name: main_calendar_events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.main_calendar_events (id, date, _owner_, repeat, end_date, end_time, event, start_time, register_people, main_calendar_events_locations_id, _timestamp_, notes, main_calendar_events_categories_id, event_id) FROM stdin;
\.


--
-- Data for Name: main_calendar_events_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.main_calendar_events_categories (id, text, color) FROM stdin;
\.


--
-- Data for Name: main_calendar_events_locations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.main_calendar_events_locations (id, text) FROM stdin;
\.


--
-- Data for Name: main_calendar_events_registrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.main_calendar_events_registrations (id, main_calendar_events_id, person, note) FROM stdin;
\.


--
-- Data for Name: main_calendar_events_reminders; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.main_calendar_events_reminders (id, main_calendar_events_id, note, email, _owner_, before, num, unit, repeat_days) FROM stdin;
\.


--
-- Data for Name: meal_events; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.meal_events (id, date, additional_cooks, adult_budget_amount, adult_price, assist, clean1, clean2, kid_budget_amount, kid_price, closed, max_people, menu, _owner_, setup, start_time, invoiced, under_five_budget_amount, under_five_price, notes, end_time, closed_message, time_to_automatically_close, close_days_before_meal, _timestamp_) FROM stdin;
\.


--
-- Data for Name: meal_people; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.meal_people (id, meal_events_id, num_adults, num_kids, num_under_fives, dish, households_id, notes) FROM stdin;
\.


--
-- Data for Name: meetings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.meetings (id, title, days, _owner_) FROM stdin;
\.


--
-- Data for Name: meetings_people; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.meetings_people (meetings_id, people_id, color, days) FROM stdin;
\.


--
-- Data for Name: menu; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.menu (id, name, _order_) FROM stdin;
6	Calendars	1
12	Documents	2
2	Email	3
10	Groups	4
1	People	5
11	Pictures	6
3	Other	7
\.


--
-- Data for Name: menu_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.menu_items (id, people_id, name, _order_) FROM stdin;
\.


--
-- Data for Name: minutes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.minutes (id, date, summary, text, _timestamp_, _owner_, likes, show_on_news_feed, tsvector, groups_id, show_on_main_minutes_page) FROM stdin;
\.


--
-- Data for Name: minutes_attachments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.minutes_attachments (id, minutes_id, filename) FROM stdin;
\.


--
-- Data for Name: minutes_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.minutes_comments (id, minutes_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.movies (id, title, genre, format, owner, on_loan_to, comments) FROM stdin;
\.


--
-- Data for Name: news; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news (id, provider, item_id, _owner_, _timestamp_, last_update, channel) FROM stdin;
\.


--
-- Data for Name: news_feed; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news_feed (id, _owner_, text, _timestamp_, likes, show_on_news_feed) FROM stdin;
\.


--
-- Data for Name: news_feed_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.news_feed_comments (id, news_feed_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: notes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notes (id, households_id, title, note, category, public) FROM stdin;
\.


--
-- Data for Name: notices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notices (id, category, date, description, _owner_, title, likes, show_on_news_feed) FROM stdin;
\.


--
-- Data for Name: notices_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notices_comments (id, notices_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: notices_pictures; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.notices_pictures (id, notices_id, filename) FROM stdin;
\.


--
-- Data for Name: offices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.offices (id, office, description, notes) FROM stdin;
\.


--
-- Data for Name: offices_history; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.offices_history (id, person, start_date, end_date, notes, offices_id) FROM stdin;
\.


--
-- Data for Name: pages; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pages (id, embed, enabled, name, text, url, public, roles) FROM stdin;
4	f	t	Discussions	\N	/Discussions	f	\N
5	f	t	Household	\N	/Household	f	\N
9	f	t	Documents	\N	/Documents	f	\N
10	f	t	Home	\N	/Home	f	\N
11	f	t	Meeting Scheduler	\N	/Meeting+Scheduler	f	\N
13	f	t	Notices	\N	/Notices	f	\N
16	f	t	Reviews	\N	/Reviews	f	\N
17	f	t	Other	\N	Other	f	\N
21	f	t	Calendars	\N	/Calendars	f	\N
22	f	t	Email	\N	/Email	f	\N
3	f	t	Accounting	\N	/Accounting	f	\N
1	f	t	Decisions	\N	/Decisions	f	\N
8	f	t	Groups	\N	/Groups	f	\N
12	f	t	Minutes	\N	/Minutes	f	\N
2	f	t	People	\N	/People	f	\N
6	f	t	Pets	\N	/Pets	f	\N
14	f	t	Pictures	\N	/Pictures	f	\N
15	f	t	Recipes	\N	/Recipes	f	\N
18	f	t	Surveys	\N	/Surveys	f	\N
19	f	t	Time Bank	\N	/Time+Bank	f	\N
7	f	t	Vehicles	\N	/Vehicles	f	\N
20	f	t	Work	\N	/Work	f	\N
23	f	t	Contacts	\N	/Contacts	f	\N
24	f	t	Bicycles	\N	/Bicycles	f	\N
25	f	t	Lending	\N	/Lending	f	\N
\.


--
-- Data for Name: payees; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payees (id, text) FROM stdin;
\.


--
-- Data for Name: people; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.people (id, first, last, email, user_name, password, active, address, birthday, households_id, middle, phone, theme, resident, last_login, owner, picture, bio, show_birthday_on_calendar, data, data_keys, uuid, timezone, can_post_to_mail_lists, emergency_contacts, pronouns, send_email_when_someone_comments_on_my_posts, json, date_added, send_email_when_someone_posts_to_the_site, reset_id, remove_me_when_i_post_to_lists, hide_email_on_site, must_change_credentials) FROM stdin;
1	Administrator	\N	\N	admin	e1b6f00013bb86d002664316b3e95e59	f	\N	\N	0	\N	\N	cerulean	f	2024-08-12 14:12:00	f	\N	\N	t	\N	\N	118192f1-4208-4d79-8a5f-7ea7019fca5a	\N	t	\N	\N	t	\N	2022-07-05	f	\N	f	f	f
\.


--
-- Data for Name: people_bio_pictures; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.people_bio_pictures (id, people_id, filename) FROM stdin;
\.


--
-- Data for Name: people_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.people_groups (people_id, groups_id) FROM stdin;
\.


--
-- Data for Name: people_mail_lists; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.people_mail_lists (id, people_id, name, people_ids) FROM stdin;
\.


--
-- Data for Name: people_skills; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.people_skills (id, people_id, skill) FROM stdin;
\.


--
-- Data for Name: people_work_tasks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.people_work_tasks (people_id, work_tasks_id, date, hours) FROM stdin;
\.


--
-- Data for Name: pets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pets (id, households_id, name, notes, photo) FROM stdin;
\.


--
-- Data for Name: pictures; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pictures (id, file, caption, _owner_, _timestamp_, width, height, likes, can_show_on_login_page, pictures_galleries_id, show_on_news_feed) FROM stdin;
\.


--
-- Data for Name: pictures_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pictures_comments (id, pictures_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: pictures_galleries; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pictures_galleries (id, text) FROM stdin;
\.


--
-- Data for Name: pictures_pictures_tags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pictures_pictures_tags (pictures_id, pictures_tags_id) FROM stdin;
\.


--
-- Data for Name: pictures_tags; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pictures_tags (id, tag) FROM stdin;
\.


--
-- Data for Name: recipes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipes (id, households_id, title, ingredients, directions, coho, _owner_, show_on_news_feed, recipes_categories_id) FROM stdin;
\.


--
-- Data for Name: recipes_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipes_categories (id, text) FROM stdin;
\.


--
-- Data for Name: recipes_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recipes_comments (id, recipes_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: recommendations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.recommendations (id, recommendation, recommended_by, comments) FROM stdin;
\.


--
-- Data for Name: reviews; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reviews (id, title, review, _owner_, likes, categories_id, show_on_news_feed, contact, reviews_categories_id) FROM stdin;
\.


--
-- Data for Name: reviews_attachments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reviews_attachments (id, reviews_id, filename) FROM stdin;
\.


--
-- Data for Name: reviews_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reviews_categories (id, text) FROM stdin;
1	doctors
2	electricians
3	plumbers
4	restaurants
5	movies
6	books
7	websites
8	handypeople
\.


--
-- Data for Name: reviews_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reviews_comments (id, reviews_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: subscribers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.subscribers (id, mail_lists_id, email, name) FROM stdin;
\.


--
-- Data for Name: survey_answers; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.survey_answers (id, survey_items_id, answer, _owner_) FROM stdin;
\.


--
-- Data for Name: survey_items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.survey_items (id, surveys_id, type, type_data, text, pre_text, post_text, required, _order_) FROM stdin;
\.


--
-- Data for Name: survey_questions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.survey_questions (id, surveys_id, question, answer_type, type_data, required, pre_text, post_text, _order_, _timestamp_) FROM stdin;
\.


--
-- Data for Name: surveys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.surveys (id, title, description, number_questions, show_answers, show_names, _owner_, closed, close_date) FROM stdin;
\.


--
-- Data for Name: tasks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tasks (id, description, notes, num_people_needed, workers, _owner_, groups_id) FROM stdin;
\.


--
-- Data for Name: timebank; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.timebank (id, date, hours, _owner_, task, recipient) FROM stdin;
\.


--
-- Data for Name: timebank_comments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.timebank_comments (id, timebank_id, reply_to, comment, _timestamp_, _owner_) FROM stdin;
\.


--
-- Data for Name: timebank_requests; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.timebank_requests (id, description, _owner_) FROM stdin;
\.


--
-- Data for Name: todos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.todos (id, households_id, task, notes, assigned_to, project) FROM stdin;
\.


--
-- Data for Name: transactions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.transactions (id, accounts_id, amount, date, description, households_id, num, reconciled, type, transactions_id, bank_accounts_id, payees_id, notes) FROM stdin;
\.


--
-- Data for Name: units; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.units (id, unit, hoa_dues) FROM stdin;
\.


--
-- Data for Name: user_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_roles (id, people_id, user_name, role) FROM stdin;
1	1	admin	administrator
2	1	admin	coho
\.


--
-- Data for Name: vehicles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.vehicles (id, households_id, available_for_sharing, description, license_plate, notes, parking_spot, photo) FROM stdin;
\.


--
-- Data for Name: votes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.votes (id, decisions_id, vote, _owner_, _timestamp_) FROM stdin;
\.


--
-- Data for Name: work_hours; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.work_hours (id, work_tasks_id, date, hours, _owner_, notes, link) FROM stdin;
\.


--
-- Data for Name: work_tasks; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.work_tasks (id, task, active, groups_id) FROM stdin;
\.


--
-- Name: _columns__id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public._columns__id_seq', 1, false);


--
-- Name: _modules__id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public._modules__id_seq', 8, true);


--
-- Name: _relationship_defs__id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public._relationship_defs__id_seq', 1, false);


--
-- Name: _roles__id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public._roles__id_seq', 45, true);


--
-- Name: _settings__id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public._settings__id_seq', 40, true);


--
-- Name: _tables__id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public._tables__id_seq', 1, false);


--
-- Name: _views__id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public._views__id_seq', 1, false);


--
-- Name: accounts_budgets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.accounts_budgets_id_seq', 1, false);


--
-- Name: accounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.accounts_id_seq', 2, true);


--
-- Name: additional_emails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.additional_emails_id_seq', 1, false);


--
-- Name: additional_senders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.additional_senders_id_seq', 1, false);


--
-- Name: automatic_transactions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.automatic_transactions_id_seq', 1, false);


--
-- Name: bank_accounts_balances_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bank_accounts_balances_id_seq', 1, false);


--
-- Name: bank_accounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bank_accounts_id_seq', 1, false);


--
-- Name: bicycles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bicycles_id_seq', 1, false);


--
-- Name: birthday_reminders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.birthday_reminders_id_seq', 1, false);


--
-- Name: birthdays_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.birthdays_events_id_seq', 1, false);


--
-- Name: blackboard_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.blackboard_id_seq', 1, false);


--
-- Name: budgets_amounts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.budgets_amounts_id_seq', 1, false);


--
-- Name: budgets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.budgets_id_seq', 1, false);


--
-- Name: calendar_guest_room_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.calendar_guest_room_events_id_seq', 1, false);


--
-- Name: contacts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.contacts_id_seq', 1, false);


--
-- Name: decisions_agree_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.decisions_agree_id_seq', 1, false);


--
-- Name: decisions_attachments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.decisions_attachments_id_seq', 1, false);


--
-- Name: decisions_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.decisions_comments_id_seq', 1, false);


--
-- Name: decisions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.decisions_id_seq', 1, false);


--
-- Name: decisions_rank_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.decisions_rank_id_seq', 1, false);


--
-- Name: discussions_attachments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.discussions_attachments_id_seq', 1, false);


--
-- Name: discussions_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.discussions_categories_id_seq', 1, false);


--
-- Name: discussions_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.discussions_comments_id_seq', 1, false);


--
-- Name: discussions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.discussions_id_seq', 1, false);


--
-- Name: documents_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documents_comments_id_seq', 1, false);


--
-- Name: documents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documents_id_seq', 2, true);


--
-- Name: documents_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.documents_types_id_seq', 12, true);


--
-- Name: equipment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.equipment_id_seq', 1, false);


--
-- Name: general_meetings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.general_meetings_id_seq', 1, false);


--
-- Name: group_links_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.group_links_id_seq', 1, false);


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.groups_id_seq', 1, false);


--
-- Name: groups_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.groups_jobs_id_seq', 1, false);


--
-- Name: guest_room_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.guest_room_events_id_seq', 1, false);


--
-- Name: homes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.homes_id_seq', 1, false);


--
-- Name: households_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.households_events_id_seq', 1, false);


--
-- Name: households_events_reminders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.households_events_reminders_id_seq', 1, false);


--
-- Name: households_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.households_id_seq', 1, false);


--
-- Name: households_pictures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.households_pictures_id_seq', 1, false);


--
-- Name: jobs_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jobs_history_id_seq', 1, false);


--
-- Name: jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jobs_id_seq', 1, false);


--
-- Name: lending_items_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lending_items_categories_id_seq', 1, false);


--
-- Name: lending_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lending_items_id_seq', 1, false);


--
-- Name: lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lists_id_seq', 1, false);


--
-- Name: locations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.locations_id_seq', 1, false);


--
-- Name: lostfound_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.lostfound_id_seq', 1, false);


--
-- Name: mail_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.mail_lists_id_seq', 1, false);


--
-- Name: main_calendar_events_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.main_calendar_events_categories_id_seq', 1, false);


--
-- Name: main_calendar_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.main_calendar_events_id_seq', 1, false);


--
-- Name: main_calendar_events_locations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.main_calendar_events_locations_id_seq', 1, false);


--
-- Name: main_calendar_events_registrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.main_calendar_events_registrations_id_seq', 1, false);


--
-- Name: main_calendar_events_reminders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.main_calendar_events_reminders_id_seq', 1, false);


--
-- Name: meal_events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.meal_events_id_seq', 1, false);


--
-- Name: meal_people_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.meal_people_id_seq', 1, false);


--
-- Name: meetings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.meetings_id_seq', 1, false);


--
-- Name: menu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.menu_id_seq', 12, true);


--
-- Name: menu_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.menu_items_id_seq', 1, false);


--
-- Name: minutes_attachments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.minutes_attachments_id_seq', 1, false);


--
-- Name: minutes_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.minutes_comments_id_seq', 1, false);


--
-- Name: minutes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.minutes_id_seq', 1, false);


--
-- Name: movies_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movies_id_seq', 1, false);


--
-- Name: news_feed_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_feed_comments_id_seq', 1, false);


--
-- Name: news_feed_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_feed_id_seq', 1, false);


--
-- Name: news_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.news_id_seq', 1, false);


--
-- Name: notes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notes_id_seq', 1, false);


--
-- Name: notices_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notices_comments_id_seq', 1, false);


--
-- Name: notices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notices_id_seq', 1, false);


--
-- Name: notices_pictures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.notices_pictures_id_seq', 1, false);


--
-- Name: offices_history_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.offices_history_id_seq', 1, false);


--
-- Name: offices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.offices_id_seq', 1, false);


--
-- Name: pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pages_id_seq', 25, true);


--
-- Name: payees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payees_id_seq', 1, false);


--
-- Name: people_bio_pictures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.people_bio_pictures_id_seq', 1, false);


--
-- Name: people_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.people_id_seq', 2, false);


--
-- Name: people_mail_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.people_mail_lists_id_seq', 1, false);


--
-- Name: people_skills_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.people_skills_id_seq', 1, false);


--
-- Name: pets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pets_id_seq', 1, false);


--
-- Name: pictures_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pictures_comments_id_seq', 1, false);


--
-- Name: pictures_galleries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pictures_galleries_id_seq', 1, false);


--
-- Name: pictures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pictures_id_seq', 1, false);


--
-- Name: pictures_tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pictures_tags_id_seq', 1, false);


--
-- Name: recipes_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recipes_categories_id_seq', 1, false);


--
-- Name: recipes_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recipes_comments_id_seq', 1, false);


--
-- Name: recipes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recipes_id_seq', 1, false);


--
-- Name: recommendations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recommendations_id_seq', 1, false);


--
-- Name: reviews_attachments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reviews_attachments_id_seq', 1, false);


--
-- Name: reviews_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reviews_categories_id_seq', 8, true);


--
-- Name: reviews_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reviews_comments_id_seq', 1, false);


--
-- Name: reviews_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reviews_id_seq', 1, false);


--
-- Name: subscribers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.subscribers_id_seq', 1, false);


--
-- Name: survey_answers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.survey_answers_id_seq', 1, false);


--
-- Name: survey_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.survey_items_id_seq', 1, false);


--
-- Name: survey_questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.survey_questions_id_seq', 1, false);


--
-- Name: surveys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.surveys_id_seq', 1, false);


--
-- Name: tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tasks_id_seq', 1, false);


--
-- Name: timebank_comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.timebank_comments_id_seq', 1, false);


--
-- Name: timebank_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.timebank_id_seq', 1, false);


--
-- Name: timebank_requests_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.timebank_requests_id_seq', 1, false);


--
-- Name: todos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.todos_id_seq', 1, false);


--
-- Name: transactions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.transactions_id_seq', 1, false);


--
-- Name: units_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.units_id_seq', 1, false);


--
-- Name: user_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_roles_id_seq', 34, true);


--
-- Name: vehicles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.vehicles_id_seq', 1, false);


--
-- Name: votes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.votes_id_seq', 1, false);


--
-- Name: work_hours_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.work_hours_id_seq', 1, false);


--
-- Name: work_tasks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.work_tasks_id_seq', 1, false);


--
-- Name: _columns_ _columns__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._columns_
    ADD CONSTRAINT _columns__pkey PRIMARY KEY (id);


--
-- Name: _modules_ _modules__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._modules_
    ADD CONSTRAINT _modules__pkey PRIMARY KEY (id);


--
-- Name: _relationship_defs_ _relationship_defs__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._relationship_defs_
    ADD CONSTRAINT _relationship_defs__pkey PRIMARY KEY (id);


--
-- Name: _roles_ _roles__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._roles_
    ADD CONSTRAINT _roles__pkey PRIMARY KEY (id);


--
-- Name: _settings_ _settings__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._settings_
    ADD CONSTRAINT _settings__pkey PRIMARY KEY (id);


--
-- Name: _tables_ _tables__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._tables_
    ADD CONSTRAINT _tables__pkey PRIMARY KEY (id);


--
-- Name: _views_ _views__pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._views_
    ADD CONSTRAINT _views__pkey PRIMARY KEY (id);


--
-- Name: accounts_budgets accounts_budgets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts_budgets
    ADD CONSTRAINT accounts_budgets_pkey PRIMARY KEY (id);


--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: additional_emails additional_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_emails
    ADD CONSTRAINT additional_emails_pkey PRIMARY KEY (id);


--
-- Name: additional_senders additional_senders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_senders
    ADD CONSTRAINT additional_senders_pkey PRIMARY KEY (id);


--
-- Name: automatic_transactions automatic_transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.automatic_transactions
    ADD CONSTRAINT automatic_transactions_pkey PRIMARY KEY (id);


--
-- Name: bank_accounts_balances bank_accounts_balances_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_accounts_balances
    ADD CONSTRAINT bank_accounts_balances_pkey PRIMARY KEY (id);


--
-- Name: bank_accounts bank_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_accounts
    ADD CONSTRAINT bank_accounts_pkey PRIMARY KEY (id);


--
-- Name: bicycles bicycles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bicycles
    ADD CONSTRAINT bicycles_pkey PRIMARY KEY (id);


--
-- Name: birthday_reminders birthday_reminders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.birthday_reminders
    ADD CONSTRAINT birthday_reminders_pkey PRIMARY KEY (id);


--
-- Name: birthdays_events birthdays_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.birthdays_events
    ADD CONSTRAINT birthdays_events_pkey PRIMARY KEY (id);


--
-- Name: blackboard blackboard_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.blackboard
    ADD CONSTRAINT blackboard_pkey PRIMARY KEY (id);


--
-- Name: budgets_amounts budgets_amounts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.budgets_amounts
    ADD CONSTRAINT budgets_amounts_pkey PRIMARY KEY (id);


--
-- Name: budgets budgets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.budgets
    ADD CONSTRAINT budgets_pkey PRIMARY KEY (id);


--
-- Name: calendar_guest_room_events calendar_guest_room_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendar_guest_room_events
    ADD CONSTRAINT calendar_guest_room_events_pkey PRIMARY KEY (id);


--
-- Name: contacts contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contacts
    ADD CONSTRAINT contacts_pkey PRIMARY KEY (id);


--
-- Name: decisions_agree decisions_agree_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_agree
    ADD CONSTRAINT decisions_agree_pkey PRIMARY KEY (id);


--
-- Name: decisions_attachments decisions_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_attachments
    ADD CONSTRAINT decisions_attachments_pkey PRIMARY KEY (id);


--
-- Name: decisions_comments decisions_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_comments
    ADD CONSTRAINT decisions_comments_pkey PRIMARY KEY (id);


--
-- Name: decisions decisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions
    ADD CONSTRAINT decisions_pkey PRIMARY KEY (id);


--
-- Name: decisions_rank decisions_rank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_rank
    ADD CONSTRAINT decisions_rank_pkey PRIMARY KEY (id);


--
-- Name: discussions_attachments discussions_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_attachments
    ADD CONSTRAINT discussions_attachments_pkey PRIMARY KEY (id);


--
-- Name: discussions_categories discussions_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_categories
    ADD CONSTRAINT discussions_categories_pkey PRIMARY KEY (id);


--
-- Name: discussions_comments discussions_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_comments
    ADD CONSTRAINT discussions_comments_pkey PRIMARY KEY (id);


--
-- Name: discussions discussions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions
    ADD CONSTRAINT discussions_pkey PRIMARY KEY (id);


--
-- Name: documents_comments documents_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents_comments
    ADD CONSTRAINT documents_comments_pkey PRIMARY KEY (id);


--
-- Name: documents documents_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: documents_types documents_types_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents_types
    ADD CONSTRAINT documents_types_pkey PRIMARY KEY (id);


--
-- Name: equipment equipment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT equipment_pkey PRIMARY KEY (id);


--
-- Name: households_events families_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_events
    ADD CONSTRAINT families_events_pkey PRIMARY KEY (id);


--
-- Name: households_events_reminders families_events_reminders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_events_reminders
    ADD CONSTRAINT families_events_reminders_pkey PRIMARY KEY (id);


--
-- Name: households_pictures families_pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_pictures
    ADD CONSTRAINT families_pictures_pkey PRIMARY KEY (id);


--
-- Name: households families_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households
    ADD CONSTRAINT families_pkey PRIMARY KEY (id);


--
-- Name: general_meetings general_meetings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.general_meetings
    ADD CONSTRAINT general_meetings_pkey PRIMARY KEY (id);


--
-- Name: group_links group_links_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_links
    ADD CONSTRAINT group_links_pkey PRIMARY KEY (id);


--
-- Name: groups_jobs groups_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups_jobs
    ADD CONSTRAINT groups_jobs_pkey PRIMARY KEY (id);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: guest_room_events guest_room_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.guest_room_events
    ADD CONSTRAINT guest_room_events_pkey PRIMARY KEY (id);


--
-- Name: homes homes_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.homes
    ADD CONSTRAINT homes_number_key UNIQUE (number);


--
-- Name: homes homes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.homes
    ADD CONSTRAINT homes_pkey PRIMARY KEY (id);


--
-- Name: jobs_history jobs_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs_history
    ADD CONSTRAINT jobs_history_pkey PRIMARY KEY (id);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: lending_items_categories lending_items_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lending_items_categories
    ADD CONSTRAINT lending_items_categories_pkey PRIMARY KEY (id);


--
-- Name: lending_items lending_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lending_items
    ADD CONSTRAINT lending_items_pkey PRIMARY KEY (id);


--
-- Name: lists lists_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists
    ADD CONSTRAINT lists_pkey PRIMARY KEY (id);


--
-- Name: locations locations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_pkey PRIMARY KEY (id);


--
-- Name: lostfound lostfound_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lostfound
    ADD CONSTRAINT lostfound_pkey PRIMARY KEY (id);


--
-- Name: mail_lists mail_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail_lists
    ADD CONSTRAINT mail_lists_pkey PRIMARY KEY (id);


--
-- Name: main_calendar_events_categories main_calendar_events_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_categories
    ADD CONSTRAINT main_calendar_events_categories_pkey PRIMARY KEY (id);


--
-- Name: main_calendar_events_locations main_calendar_events_locations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_locations
    ADD CONSTRAINT main_calendar_events_locations_pkey PRIMARY KEY (id);


--
-- Name: main_calendar_events main_calendar_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events
    ADD CONSTRAINT main_calendar_events_pkey PRIMARY KEY (id);


--
-- Name: main_calendar_events_registrations main_calendar_events_registrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_registrations
    ADD CONSTRAINT main_calendar_events_registrations_pkey PRIMARY KEY (id);


--
-- Name: main_calendar_events_reminders main_calendar_events_reminders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_reminders
    ADD CONSTRAINT main_calendar_events_reminders_pkey PRIMARY KEY (id);


--
-- Name: meal_events meal_events_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meal_events
    ADD CONSTRAINT meal_events_pkey PRIMARY KEY (id);


--
-- Name: meal_people meal_people_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meal_people
    ADD CONSTRAINT meal_people_pkey PRIMARY KEY (id);


--
-- Name: meetings meetings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meetings
    ADD CONSTRAINT meetings_pkey PRIMARY KEY (id);


--
-- Name: menu_items menu_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items
    ADD CONSTRAINT menu_items_pkey PRIMARY KEY (id);


--
-- Name: menu menu_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (id);


--
-- Name: minutes_attachments minutes_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes_attachments
    ADD CONSTRAINT minutes_attachments_pkey PRIMARY KEY (id);


--
-- Name: minutes_comments minutes_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes_comments
    ADD CONSTRAINT minutes_comments_pkey PRIMARY KEY (id);


--
-- Name: minutes minutes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes
    ADD CONSTRAINT minutes_pkey PRIMARY KEY (id);


--
-- Name: movies movies_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (id);


--
-- Name: news_feed_comments news_feed_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_feed_comments
    ADD CONSTRAINT news_feed_comments_pkey PRIMARY KEY (id);


--
-- Name: news_feed news_feed_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_feed
    ADD CONSTRAINT news_feed_pkey PRIMARY KEY (id);


--
-- Name: news news_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news_pkey PRIMARY KEY (id);


--
-- Name: notes notes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notes
    ADD CONSTRAINT notes_pkey PRIMARY KEY (id);


--
-- Name: notices_comments notices_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices_comments
    ADD CONSTRAINT notices_comments_pkey PRIMARY KEY (id);


--
-- Name: notices_pictures notices_pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices_pictures
    ADD CONSTRAINT notices_pictures_pkey PRIMARY KEY (id);


--
-- Name: notices notices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices
    ADD CONSTRAINT notices_pkey PRIMARY KEY (id);


--
-- Name: offices_history offices_history_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices_history
    ADD CONSTRAINT offices_history_pkey PRIMARY KEY (id);


--
-- Name: offices offices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices
    ADD CONSTRAINT offices_pkey PRIMARY KEY (id);


--
-- Name: pages pages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: payees payees_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payees
    ADD CONSTRAINT payees_pkey PRIMARY KEY (id);


--
-- Name: people_bio_pictures people_bio_pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_bio_pictures
    ADD CONSTRAINT people_bio_pictures_pkey PRIMARY KEY (id);


--
-- Name: people_mail_lists people_mail_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_mail_lists
    ADD CONSTRAINT people_mail_lists_pkey PRIMARY KEY (id);


--
-- Name: people people_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);


--
-- Name: people_skills people_skills_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_skills
    ADD CONSTRAINT people_skills_pkey PRIMARY KEY (id);


--
-- Name: people people_user_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_user_name_key UNIQUE (user_name);


--
-- Name: pets pets_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pets
    ADD CONSTRAINT pets_pkey PRIMARY KEY (id);


--
-- Name: pictures_comments pictures_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_comments
    ADD CONSTRAINT pictures_comments_pkey PRIMARY KEY (id);


--
-- Name: pictures_galleries pictures_galleries_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_galleries
    ADD CONSTRAINT pictures_galleries_pkey PRIMARY KEY (id);


--
-- Name: pictures pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures_pkey PRIMARY KEY (id);


--
-- Name: pictures_tags pictures_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_tags
    ADD CONSTRAINT pictures_tags_pkey PRIMARY KEY (id);


--
-- Name: recipes_categories recipes_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes_categories
    ADD CONSTRAINT recipes_categories_pkey PRIMARY KEY (id);


--
-- Name: recipes_comments recipes_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes_comments
    ADD CONSTRAINT recipes_comments_pkey PRIMARY KEY (id);


--
-- Name: recipes recipes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes
    ADD CONSTRAINT recipes_pkey PRIMARY KEY (id);


--
-- Name: recommendations recommendations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recommendations
    ADD CONSTRAINT recommendations_pkey PRIMARY KEY (id);


--
-- Name: reviews_attachments reviews_attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_attachments
    ADD CONSTRAINT reviews_attachments_pkey PRIMARY KEY (id);


--
-- Name: reviews_categories reviews_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_categories
    ADD CONSTRAINT reviews_categories_pkey PRIMARY KEY (id);


--
-- Name: reviews_comments reviews_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_comments
    ADD CONSTRAINT reviews_comments_pkey PRIMARY KEY (id);


--
-- Name: reviews reviews_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_pkey PRIMARY KEY (id);


--
-- Name: subscribers subscribers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscribers
    ADD CONSTRAINT subscribers_pkey PRIMARY KEY (id);


--
-- Name: survey_answers survey_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_answers
    ADD CONSTRAINT survey_answers_pkey PRIMARY KEY (id);


--
-- Name: survey_items survey_items_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_items
    ADD CONSTRAINT survey_items_pkey PRIMARY KEY (id);


--
-- Name: survey_questions survey_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_questions
    ADD CONSTRAINT survey_questions_pkey PRIMARY KEY (id);


--
-- Name: surveys surveys_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.surveys
    ADD CONSTRAINT surveys_pkey PRIMARY KEY (id);


--
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: timebank_comments timebank_comments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank_comments
    ADD CONSTRAINT timebank_comments_pkey PRIMARY KEY (id);


--
-- Name: timebank timebank_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank
    ADD CONSTRAINT timebank_pkey PRIMARY KEY (id);


--
-- Name: timebank_requests timebank_requests_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank_requests
    ADD CONSTRAINT timebank_requests_pkey PRIMARY KEY (id);


--
-- Name: todos todos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.todos
    ADD CONSTRAINT todos_pkey PRIMARY KEY (id);


--
-- Name: transactions transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT transactions_pkey PRIMARY KEY (id);


--
-- Name: units units_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.units
    ADD CONSTRAINT units_pkey PRIMARY KEY (id);


--
-- Name: user_roles user_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);


--
-- Name: vehicles vehicles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehicles
    ADD CONSTRAINT vehicles_pkey PRIMARY KEY (id);


--
-- Name: votes votes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.votes
    ADD CONSTRAINT votes_pkey PRIMARY KEY (id);


--
-- Name: work_hours work_hours_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work_hours
    ADD CONSTRAINT work_hours_pkey PRIMARY KEY (id);


--
-- Name: work_tasks work_tasks_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work_tasks
    ADD CONSTRAINT work_tasks_pkey PRIMARY KEY (id);


--
-- Name: _columns___views__id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX _columns___views__id ON public._columns_ USING btree (_views__id);


--
-- Name: _relationship_defs___views__id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX _relationship_defs___views__id ON public._relationship_defs_ USING btree (_views__id);


--
-- Name: accounts_budgets_accounts_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX accounts_budgets_accounts_id ON public.accounts_budgets USING btree (accounts_id);


--
-- Name: additional_emails_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX additional_emails_people_id ON public.additional_emails USING btree (people_id);


--
-- Name: bank_accounts_balances_bank_accounts_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX bank_accounts_balances_bank_accounts_id ON public.bank_accounts_balances USING btree (bank_accounts_id);


--
-- Name: bicycles_households_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX bicycles_households_id ON public.bicycles USING btree (households_id);


--
-- Name: birthday_reminders__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX birthday_reminders__owner_ ON public.birthday_reminders USING btree (_owner_);


--
-- Name: birthdays_events_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX birthdays_events_date ON public.birthdays_events USING btree (date);


--
-- Name: budgets_amounts_budgets_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX budgets_amounts_budgets_id ON public.budgets_amounts USING btree (budgets_id);


--
-- Name: calendar_guest_room_events__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX calendar_guest_room_events__owner_ ON public.calendar_guest_room_events USING btree (_owner_);


--
-- Name: calendar_guest_room_events_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX calendar_guest_room_events_date ON public.calendar_guest_room_events USING btree (date);


--
-- Name: calendar_guest_room_events_event_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX calendar_guest_room_events_event_id ON public.calendar_guest_room_events USING btree (event_id);


--
-- Name: contacts_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX contacts_families_id ON public.contacts USING btree (households_id);


--
-- Name: contacts_groups_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX contacts_groups_id ON public.contacts USING btree (groups_id);


--
-- Name: decisions__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX decisions__owner_ ON public.decisions USING btree (_owner_);


--
-- Name: decisions_agree_decisions_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX decisions_agree_decisions_id ON public.decisions_agree USING btree (decisions_id);


--
-- Name: decisions_agree_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX decisions_agree_people_id ON public.decisions_agree USING btree (people_id);


--
-- Name: decisions_attachments_decisions_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX decisions_attachments_decisions_id ON public.decisions_attachments USING btree (decisions_id);


--
-- Name: decisions_comments_decisions_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX decisions_comments_decisions_id ON public.decisions_comments USING btree (decisions_id);


--
-- Name: decisions_groups_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX decisions_groups_id ON public.decisions USING btree (groups_id);


--
-- Name: discussions_attachments_discussions_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discussions_attachments_discussions_id ON public.discussions_attachments USING btree (discussions_id);


--
-- Name: discussions_comments_discussions_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discussions_comments_discussions_id ON public.discussions_comments USING btree (discussions_id);


--
-- Name: discussions_discussions_categories_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX discussions_discussions_categories_id ON public.discussions USING btree (discussions_categories_id);


--
-- Name: documents__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX documents__owner_ ON public.documents USING btree (_owner_);


--
-- Name: documents_comments_documents_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX documents_comments_documents_id ON public.documents_comments USING btree (documents_id);


--
-- Name: documents_groups_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX documents_groups_id ON public.documents USING btree (groups_id);


--
-- Name: families_events__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX families_events__owner_ ON public.households_events USING btree (_owner_);


--
-- Name: families_events_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX families_events_date ON public.households_events USING btree (date);


--
-- Name: families_events_event_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX families_events_event_id ON public.households_events USING btree (event_id);


--
-- Name: families_events_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX families_events_families_id ON public.households_events USING btree (households_id);


--
-- Name: families_events_reminders__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX families_events_reminders__owner_ ON public.households_events_reminders USING btree (_owner_);


--
-- Name: families_events_reminders_families_events_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX families_events_reminders_families_events_id ON public.households_events_reminders USING btree (households_events_id);


--
-- Name: families_homes_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX families_homes_id ON public.households USING btree (homes_id);


--
-- Name: families_pictures_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX families_pictures_families_id ON public.households_pictures USING btree (households_id);


--
-- Name: group_links_groups_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX group_links_groups_id ON public.group_links USING btree (groups_id);


--
-- Name: groups_jobs_groups_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX groups_jobs_groups_id ON public.groups_jobs USING btree (groups_id);


--
-- Name: groups_jobs_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX groups_jobs_people_id ON public.groups_jobs USING btree (people_id);


--
-- Name: guest_room_events__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX guest_room_events__owner_ ON public.guest_room_events USING btree (_owner_);


--
-- Name: guest_room_events_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX guest_room_events_date ON public.guest_room_events USING btree (date);


--
-- Name: guest_room_events_event_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX guest_room_events_event_id ON public.guest_room_events USING btree (event_id);


--
-- Name: homes_pays_hoa; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX homes_pays_hoa ON public.homes USING btree (pays_hoa);


--
-- Name: homes_pays_water; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX homes_pays_water ON public.homes USING btree (pays_water);


--
-- Name: households_events_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX households_events_date ON public.households_events USING btree (date);


--
-- Name: households_events_event_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX households_events_event_id ON public.households_events USING btree (event_id);


--
-- Name: households_homes_homes_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX households_homes_homes_id ON public.households_homes USING btree (homes_id);


--
-- Name: households_homes_households_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX households_homes_households_id ON public.households_homes USING btree (households_id);


--
-- Name: households_pictures_households_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX households_pictures_households_id ON public.households_pictures USING btree (households_id);


--
-- Name: jobs_history_jobs_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX jobs_history_jobs_id ON public.jobs_history USING btree (jobs_id);


--
-- Name: jobs_history_person; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX jobs_history_person ON public.jobs_history USING btree (person);


--
-- Name: lending_items__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lending_items__owner_ ON public.lending_items USING btree (_owner_);


--
-- Name: lending_items_leant_to; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lending_items_leant_to ON public.lending_items USING btree (leant_to);


--
-- Name: lending_items_lending_items_categories_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lending_items_lending_items_categories_id ON public.lending_items USING btree (lending_items_categories_id);


--
-- Name: lists_groups_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX lists_groups_id ON public.lists USING btree (groups_id);


--
-- Name: mail_lists_people_mail_lists_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX mail_lists_people_mail_lists_id ON public.mail_lists_people USING btree (mail_lists_id);


--
-- Name: mail_lists_people_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX mail_lists_people_people_id ON public.mail_lists_people USING btree (people_id);


--
-- Name: main_calendar_events__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX main_calendar_events__owner_ ON public.main_calendar_events USING btree (_owner_);


--
-- Name: main_calendar_events_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX main_calendar_events_date ON public.main_calendar_events USING btree (date);


--
-- Name: main_calendar_events_event_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX main_calendar_events_event_id ON public.main_calendar_events USING btree (event_id);


--
-- Name: main_calendar_events_main_calendar_events_categories_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX main_calendar_events_main_calendar_events_categories_id ON public.main_calendar_events USING btree (main_calendar_events_categories_id);


--
-- Name: main_calendar_events_main_calendar_events_locations_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX main_calendar_events_main_calendar_events_locations_id ON public.main_calendar_events USING btree (main_calendar_events_locations_id);


--
-- Name: main_calendar_events_registrations_main_calendar_events_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX main_calendar_events_registrations_main_calendar_events_id ON public.main_calendar_events_registrations USING btree (main_calendar_events_id);


--
-- Name: main_calendar_events_reminders_main_calendar_events_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX main_calendar_events_reminders_main_calendar_events_id ON public.main_calendar_events_reminders USING btree (main_calendar_events_id);


--
-- Name: meal_events__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX meal_events__owner_ ON public.meal_events USING btree (_owner_);


--
-- Name: meal_events_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX meal_events_date ON public.meal_events USING btree (date);


--
-- Name: meal_people_meal_events_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX meal_people_meal_events_id ON public.meal_people USING btree (meal_events_id);


--
-- Name: meetings_people_meetings_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX meetings_people_meetings_id ON public.meetings_people USING btree (meetings_id);


--
-- Name: meetings_people_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX meetings_people_people_id ON public.meetings_people USING btree (people_id);


--
-- Name: menu_items_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX menu_items_people_id ON public.menu_items USING btree (people_id);


--
-- Name: minutes__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX minutes__owner_ ON public.minutes USING btree (_owner_);


--
-- Name: minutes_attachments_minutes_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX minutes_attachments_minutes_id ON public.minutes_attachments USING btree (minutes_id);


--
-- Name: minutes_comments_minutes_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX minutes_comments_minutes_id ON public.minutes_comments USING btree (minutes_id);


--
-- Name: minutes_groups_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX minutes_groups_id ON public.minutes USING btree (groups_id);


--
-- Name: news__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX news__owner_ ON public.news USING btree (_owner_);


--
-- Name: news_feed__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX news_feed__owner_ ON public.news_feed USING btree (_owner_);


--
-- Name: news_feed_comments_news_feed_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX news_feed_comments_news_feed_id ON public.news_feed_comments USING btree (news_feed_id);


--
-- Name: notes_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX notes_families_id ON public.notes USING btree (households_id);


--
-- Name: notices_comments_notices_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX notices_comments_notices_id ON public.notices_comments USING btree (notices_id);


--
-- Name: notices_pictures_notices_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX notices_pictures_notices_id ON public.notices_pictures USING btree (notices_id);


--
-- Name: offices_history_offices_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX offices_history_offices_id ON public.offices_history USING btree (offices_id);


--
-- Name: offices_history_person; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX offices_history_person ON public.offices_history USING btree (person);


--
-- Name: people_bio_pictures_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX people_bio_pictures_people_id ON public.people_bio_pictures USING btree (people_id);


--
-- Name: people_groups_groups_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX people_groups_groups_id ON public.people_groups USING btree (groups_id);


--
-- Name: people_groups_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX people_groups_people_id ON public.people_groups USING btree (people_id);


--
-- Name: people_mail_lists_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX people_mail_lists_people_id ON public.people_mail_lists USING btree (people_id);


--
-- Name: people_skills_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX people_skills_people_id ON public.people_skills USING btree (people_id);


--
-- Name: people_work_tasks_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX people_work_tasks_people_id ON public.people_work_tasks USING btree (people_id);


--
-- Name: people_work_tasks_work_tasks_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX people_work_tasks_work_tasks_id ON public.people_work_tasks USING btree (work_tasks_id);


--
-- Name: pets_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pets_families_id ON public.pets USING btree (households_id);


--
-- Name: pictures__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pictures__owner_ ON public.pictures USING btree (_owner_);


--
-- Name: pictures_comments_pictures_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pictures_comments_pictures_id ON public.pictures_comments USING btree (pictures_id);


--
-- Name: pictures_pictures_galleries_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pictures_pictures_galleries_id ON public.pictures USING btree (pictures_galleries_id);


--
-- Name: pictures_pictures_tags_pictures_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pictures_pictures_tags_pictures_id ON public.pictures_pictures_tags USING btree (pictures_id);


--
-- Name: pictures_pictures_tags_pictures_tags_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pictures_pictures_tags_pictures_tags_id ON public.pictures_pictures_tags USING btree (pictures_tags_id);


--
-- Name: recipes_comments_recipes_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX recipes_comments_recipes_id ON public.recipes_comments USING btree (recipes_id);


--
-- Name: recipes_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX recipes_families_id ON public.recipes USING btree (households_id);


--
-- Name: recipes_recipes_categories_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX recipes_recipes_categories_id ON public.recipes USING btree (recipes_categories_id);


--
-- Name: reviews__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reviews__owner_ ON public.reviews USING btree (_owner_);


--
-- Name: reviews_attachments_reviews_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reviews_attachments_reviews_id ON public.reviews_attachments USING btree (reviews_id);


--
-- Name: reviews_comments_reviews_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reviews_comments_reviews_id ON public.reviews_comments USING btree (reviews_id);


--
-- Name: reviews_reviews_categories_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX reviews_reviews_categories_id ON public.reviews USING btree (reviews_categories_id);


--
-- Name: subscribers_mail_lists_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX subscribers_mail_lists_id ON public.subscribers USING btree (mail_lists_id);


--
-- Name: survey_answers__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX survey_answers__owner_ ON public.survey_answers USING btree (_owner_);


--
-- Name: survey_answers_survey_items_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX survey_answers_survey_items_id ON public.survey_answers USING btree (survey_items_id);


--
-- Name: survey_items_surveys_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX survey_items_surveys_id ON public.survey_items USING btree (surveys_id);


--
-- Name: survey_questions_surveys_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX survey_questions_surveys_id ON public.survey_questions USING btree (surveys_id);


--
-- Name: tasks__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX tasks__owner_ ON public.tasks USING btree (_owner_);


--
-- Name: timebank__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX timebank__owner_ ON public.timebank USING btree (_owner_);


--
-- Name: timebank_comments_timebank_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX timebank_comments_timebank_id ON public.timebank_comments USING btree (timebank_id);


--
-- Name: timebank_recipient; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX timebank_recipient ON public.timebank USING btree (recipient);


--
-- Name: timebank_requests__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX timebank_requests__owner_ ON public.timebank_requests USING btree (_owner_);


--
-- Name: todos_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX todos_families_id ON public.todos USING btree (households_id);


--
-- Name: todos_households_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX todos_households_id ON public.todos USING btree (households_id);


--
-- Name: transactions_bank_accounts_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX transactions_bank_accounts_id ON public.transactions USING btree (bank_accounts_id);


--
-- Name: transactions_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX transactions_date ON public.transactions USING btree (date);


--
-- Name: transactions_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX transactions_families_id ON public.transactions USING btree (households_id);


--
-- Name: transactions_households_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX transactions_households_id ON public.transactions USING btree (households_id);


--
-- Name: transactions_transactions_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX transactions_transactions_id ON public.transactions USING btree (transactions_id);


--
-- Name: user_roles_people_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX user_roles_people_id ON public.user_roles USING btree (people_id);


--
-- Name: vehicles_families_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX vehicles_families_id ON public.vehicles USING btree (households_id);


--
-- Name: votes__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX votes__owner_ ON public.votes USING btree (_owner_);


--
-- Name: votes_decisions_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX votes_decisions_id ON public.votes USING btree (decisions_id);


--
-- Name: work_hours__owner_; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX work_hours__owner_ ON public.work_hours USING btree (_owner_);


--
-- Name: work_hours_date; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX work_hours_date ON public.work_hours USING btree (date);


--
-- Name: work_hours_work_tasks_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX work_hours_work_tasks_id ON public.work_hours USING btree (work_tasks_id);


--
-- Name: _columns_ _columns___views__id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._columns_
    ADD CONSTRAINT _columns___views__id_fkey FOREIGN KEY (_views__id) REFERENCES public._views_(id) ON DELETE CASCADE;


--
-- Name: _relationship_defs_ _relationship_defs___views__id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public._relationship_defs_
    ADD CONSTRAINT _relationship_defs___views__id_fkey FOREIGN KEY (_views__id) REFERENCES public._views_(id) ON DELETE CASCADE;


--
-- Name: accounts_budgets accounts_budgets_accounts_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.accounts_budgets
    ADD CONSTRAINT accounts_budgets_accounts_id_fkey FOREIGN KEY (accounts_id) REFERENCES public.accounts(id) ON DELETE CASCADE;


--
-- Name: additional_emails additional_emails_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.additional_emails
    ADD CONSTRAINT additional_emails_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: bank_accounts_balances bank_accounts_balances_bank_accounts_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bank_accounts_balances
    ADD CONSTRAINT bank_accounts_balances_bank_accounts_id_fkey FOREIGN KEY (bank_accounts_id) REFERENCES public.bank_accounts(id) ON DELETE CASCADE;


--
-- Name: bicycles bicycles_households_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bicycles
    ADD CONSTRAINT bicycles_households_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: birthday_reminders birthday_reminders__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.birthday_reminders
    ADD CONSTRAINT birthday_reminders__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: budgets_amounts budgets_amounts_budgets_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.budgets_amounts
    ADD CONSTRAINT budgets_amounts_budgets_id_fkey FOREIGN KEY (budgets_id) REFERENCES public.budgets(id) ON DELETE CASCADE;


--
-- Name: calendar_guest_room_events calendar_guest_room_events__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.calendar_guest_room_events
    ADD CONSTRAINT calendar_guest_room_events__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: contacts contacts_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contacts
    ADD CONSTRAINT contacts_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: contacts contacts_groups_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.contacts
    ADD CONSTRAINT contacts_groups_id_fkey FOREIGN KEY (groups_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: decisions decisions__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions
    ADD CONSTRAINT decisions__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: decisions_agree decisions_agree_decisions_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_agree
    ADD CONSTRAINT decisions_agree_decisions_id_fkey FOREIGN KEY (decisions_id) REFERENCES public.decisions(id) ON DELETE CASCADE;


--
-- Name: decisions_agree decisions_agree_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_agree
    ADD CONSTRAINT decisions_agree_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: decisions_attachments decisions_attachments_decisions_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_attachments
    ADD CONSTRAINT decisions_attachments_decisions_id_fkey FOREIGN KEY (decisions_id) REFERENCES public.decisions(id) ON DELETE CASCADE;


--
-- Name: decisions_comments decisions_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_comments
    ADD CONSTRAINT decisions_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: decisions_comments decisions_comments_decisions_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions_comments
    ADD CONSTRAINT decisions_comments_decisions_id_fkey FOREIGN KEY (decisions_id) REFERENCES public.decisions(id) ON DELETE CASCADE;


--
-- Name: decisions decisions_groups_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.decisions
    ADD CONSTRAINT decisions_groups_id_fkey FOREIGN KEY (groups_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: discussions discussions__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions
    ADD CONSTRAINT discussions__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: discussions_attachments discussions_attachments_discussions_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_attachments
    ADD CONSTRAINT discussions_attachments_discussions_id_fkey FOREIGN KEY (discussions_id) REFERENCES public.discussions(id) ON DELETE CASCADE;


--
-- Name: discussions_comments discussions_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_comments
    ADD CONSTRAINT discussions_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: discussions_comments discussions_comments_discussions_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions_comments
    ADD CONSTRAINT discussions_comments_discussions_id_fkey FOREIGN KEY (discussions_id) REFERENCES public.discussions(id) ON DELETE CASCADE;


--
-- Name: discussions discussions_discussions_categories_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.discussions
    ADD CONSTRAINT discussions_discussions_categories_id_fkey FOREIGN KEY (discussions_categories_id) REFERENCES public.discussions_categories(id) ON DELETE SET NULL;


--
-- Name: documents documents__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT documents__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: documents_comments documents_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents_comments
    ADD CONSTRAINT documents_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: documents_comments documents_comments_documents_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents_comments
    ADD CONSTRAINT documents_comments_documents_id_fkey FOREIGN KEY (documents_id) REFERENCES public.documents(id) ON DELETE CASCADE;


--
-- Name: documents documents_groups_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT documents_groups_id_fkey FOREIGN KEY (groups_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: households_events families_events__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_events
    ADD CONSTRAINT families_events__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: households_events families_events_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_events
    ADD CONSTRAINT families_events_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: households_events_reminders families_events_reminders__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_events_reminders
    ADD CONSTRAINT families_events_reminders__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: households_events_reminders families_events_reminders_families_events_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_events_reminders
    ADD CONSTRAINT families_events_reminders_families_events_id_fkey FOREIGN KEY (households_events_id) REFERENCES public.households_events(id) ON DELETE CASCADE;


--
-- Name: households families_homes_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households
    ADD CONSTRAINT families_homes_id_fkey FOREIGN KEY (homes_id) REFERENCES public.homes(id) ON DELETE SET NULL;


--
-- Name: households_pictures families_pictures_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_pictures
    ADD CONSTRAINT families_pictures_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: group_links group_links_groups_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.group_links
    ADD CONSTRAINT group_links_groups_id_fkey FOREIGN KEY (groups_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: groups_jobs groups_jobs_groups_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups_jobs
    ADD CONSTRAINT groups_jobs_groups_id_fkey FOREIGN KEY (groups_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: groups_jobs groups_jobs_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.groups_jobs
    ADD CONSTRAINT groups_jobs_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: guest_room_events guest_room_events__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.guest_room_events
    ADD CONSTRAINT guest_room_events__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: homes homes_pays_hoa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.homes
    ADD CONSTRAINT homes_pays_hoa_fkey FOREIGN KEY (pays_hoa) REFERENCES public.households(id) ON DELETE SET NULL;


--
-- Name: homes homes_pays_water_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.homes
    ADD CONSTRAINT homes_pays_water_fkey FOREIGN KEY (pays_water) REFERENCES public.households(id) ON DELETE SET NULL;


--
-- Name: households_homes households_homes_homes_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_homes
    ADD CONSTRAINT households_homes_homes_id_fkey FOREIGN KEY (homes_id) REFERENCES public.homes(id) ON DELETE CASCADE;


--
-- Name: households_homes households_homes_households_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.households_homes
    ADD CONSTRAINT households_homes_households_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: jobs_history jobs_history_jobs_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs_history
    ADD CONSTRAINT jobs_history_jobs_id_fkey FOREIGN KEY (jobs_id) REFERENCES public.jobs(id) ON DELETE CASCADE;


--
-- Name: jobs_history jobs_history_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jobs_history
    ADD CONSTRAINT jobs_history_person_fkey FOREIGN KEY (person) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: lending_items lending_items__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lending_items
    ADD CONSTRAINT lending_items__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: lending_items lending_items_leant_to_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lending_items
    ADD CONSTRAINT lending_items_leant_to_fkey FOREIGN KEY (leant_to) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: lending_items lending_items_lending_items_categories_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lending_items
    ADD CONSTRAINT lending_items_lending_items_categories_id_fkey FOREIGN KEY (lending_items_categories_id) REFERENCES public.lending_items_categories(id) ON DELETE SET NULL;


--
-- Name: lists lists_groups_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.lists
    ADD CONSTRAINT lists_groups_id_fkey FOREIGN KEY (groups_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: mail_lists_digest mail_lists_digest_mail_lists_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail_lists_digest
    ADD CONSTRAINT mail_lists_digest_mail_lists_id_fkey FOREIGN KEY (mail_lists_id) REFERENCES public.mail_lists(id) ON DELETE CASCADE;


--
-- Name: mail_lists_digest mail_lists_digest_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail_lists_digest
    ADD CONSTRAINT mail_lists_digest_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: mail_lists_people mail_lists_people_mail_lists_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail_lists_people
    ADD CONSTRAINT mail_lists_people_mail_lists_id_fkey FOREIGN KEY (mail_lists_id) REFERENCES public.mail_lists(id) ON DELETE CASCADE;


--
-- Name: mail_lists_people mail_lists_people_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.mail_lists_people
    ADD CONSTRAINT mail_lists_people_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: main_calendar_events main_calendar_events__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events
    ADD CONSTRAINT main_calendar_events__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: main_calendar_events main_calendar_events_main_calendar_events_categories_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events
    ADD CONSTRAINT main_calendar_events_main_calendar_events_categories_id_fkey FOREIGN KEY (main_calendar_events_categories_id) REFERENCES public.main_calendar_events_categories(id) ON DELETE SET NULL;


--
-- Name: main_calendar_events main_calendar_events_main_calendar_events_locations_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events
    ADD CONSTRAINT main_calendar_events_main_calendar_events_locations_id_fkey FOREIGN KEY (main_calendar_events_locations_id) REFERENCES public.main_calendar_events_locations(id) ON DELETE SET NULL;


--
-- Name: main_calendar_events_registrations main_calendar_events_registrations_main_calendar_events_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_registrations
    ADD CONSTRAINT main_calendar_events_registrations_main_calendar_events_id_fkey FOREIGN KEY (main_calendar_events_id) REFERENCES public.main_calendar_events(id) ON DELETE CASCADE;


--
-- Name: main_calendar_events_reminders main_calendar_events_reminders__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_reminders
    ADD CONSTRAINT main_calendar_events_reminders__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: main_calendar_events_reminders main_calendar_events_reminders_main_calendar_events_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.main_calendar_events_reminders
    ADD CONSTRAINT main_calendar_events_reminders_main_calendar_events_id_fkey FOREIGN KEY (main_calendar_events_id) REFERENCES public.main_calendar_events(id) ON DELETE CASCADE;


--
-- Name: meal_events meal_events__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meal_events
    ADD CONSTRAINT meal_events__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: meal_people meal_people_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meal_people
    ADD CONSTRAINT meal_people_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: meal_people meal_people_meal_events_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meal_people
    ADD CONSTRAINT meal_people_meal_events_id_fkey FOREIGN KEY (meal_events_id) REFERENCES public.meal_events(id) ON DELETE CASCADE;


--
-- Name: meetings meetings__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meetings
    ADD CONSTRAINT meetings__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: meetings_people meetings_people_meetings_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meetings_people
    ADD CONSTRAINT meetings_people_meetings_id_fkey FOREIGN KEY (meetings_id) REFERENCES public.meetings(id) ON DELETE CASCADE;


--
-- Name: meetings_people meetings_people_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.meetings_people
    ADD CONSTRAINT meetings_people_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: menu_items menu_items_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.menu_items
    ADD CONSTRAINT menu_items_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: minutes minutes__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes
    ADD CONSTRAINT minutes__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: minutes_attachments minutes_attachments_minutes_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes_attachments
    ADD CONSTRAINT minutes_attachments_minutes_id_fkey FOREIGN KEY (minutes_id) REFERENCES public.minutes(id) ON DELETE CASCADE;


--
-- Name: minutes_comments minutes_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes_comments
    ADD CONSTRAINT minutes_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: minutes_comments minutes_comments_minutes_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes_comments
    ADD CONSTRAINT minutes_comments_minutes_id_fkey FOREIGN KEY (minutes_id) REFERENCES public.minutes(id) ON DELETE CASCADE;


--
-- Name: minutes minutes_groups_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.minutes
    ADD CONSTRAINT minutes_groups_id_fkey FOREIGN KEY (groups_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: news news__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news
    ADD CONSTRAINT news__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: news_feed news_feed__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_feed
    ADD CONSTRAINT news_feed__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: news_feed_comments news_feed_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_feed_comments
    ADD CONSTRAINT news_feed_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: news_feed_comments news_feed_comments_news_feed_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.news_feed_comments
    ADD CONSTRAINT news_feed_comments_news_feed_id_fkey FOREIGN KEY (news_feed_id) REFERENCES public.news_feed(id) ON DELETE CASCADE;


--
-- Name: notes notes_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notes
    ADD CONSTRAINT notes_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: notices_comments notices_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices_comments
    ADD CONSTRAINT notices_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: notices_comments notices_comments_notices_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices_comments
    ADD CONSTRAINT notices_comments_notices_id_fkey FOREIGN KEY (notices_id) REFERENCES public.notices(id) ON DELETE CASCADE;


--
-- Name: notices_pictures notices_pictures_notices_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notices_pictures
    ADD CONSTRAINT notices_pictures_notices_id_fkey FOREIGN KEY (notices_id) REFERENCES public.notices(id) ON DELETE CASCADE;


--
-- Name: offices_history offices_history_offices_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices_history
    ADD CONSTRAINT offices_history_offices_id_fkey FOREIGN KEY (offices_id) REFERENCES public.offices(id) ON DELETE CASCADE;


--
-- Name: offices_history offices_history_person_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.offices_history
    ADD CONSTRAINT offices_history_person_fkey FOREIGN KEY (person) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: people_bio_pictures people_bio_pictures_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_bio_pictures
    ADD CONSTRAINT people_bio_pictures_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: people_groups people_groups_groups_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_groups
    ADD CONSTRAINT people_groups_groups_id_fkey FOREIGN KEY (groups_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: people_groups people_groups_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_groups
    ADD CONSTRAINT people_groups_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: people_mail_lists people_mail_lists_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_mail_lists
    ADD CONSTRAINT people_mail_lists_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: people_skills people_skills_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_skills
    ADD CONSTRAINT people_skills_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: people_work_tasks people_work_tasks_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_work_tasks
    ADD CONSTRAINT people_work_tasks_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: people_work_tasks people_work_tasks_work_tasks_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.people_work_tasks
    ADD CONSTRAINT people_work_tasks_work_tasks_id_fkey FOREIGN KEY (work_tasks_id) REFERENCES public.work_tasks(id) ON DELETE CASCADE;


--
-- Name: pets pets_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pets
    ADD CONSTRAINT pets_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: pictures pictures__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: pictures_comments pictures_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_comments
    ADD CONSTRAINT pictures_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: pictures_comments pictures_comments_pictures_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_comments
    ADD CONSTRAINT pictures_comments_pictures_id_fkey FOREIGN KEY (pictures_id) REFERENCES public.pictures(id) ON DELETE CASCADE;


--
-- Name: pictures pictures_pictures_galleries_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures_pictures_galleries_id_fkey FOREIGN KEY (pictures_galleries_id) REFERENCES public.pictures_galleries(id) ON DELETE SET NULL;


--
-- Name: pictures_pictures_tags pictures_pictures_tags_pictures_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_pictures_tags
    ADD CONSTRAINT pictures_pictures_tags_pictures_id_fkey FOREIGN KEY (pictures_id) REFERENCES public.pictures(id) ON DELETE CASCADE;


--
-- Name: pictures_pictures_tags pictures_pictures_tags_pictures_tags_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures_pictures_tags
    ADD CONSTRAINT pictures_pictures_tags_pictures_tags_id_fkey FOREIGN KEY (pictures_tags_id) REFERENCES public.pictures_tags(id) ON DELETE CASCADE;


--
-- Name: recipes recipes__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes
    ADD CONSTRAINT recipes__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: recipes_comments recipes_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes_comments
    ADD CONSTRAINT recipes_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: recipes_comments recipes_comments_recipes_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes_comments
    ADD CONSTRAINT recipes_comments_recipes_id_fkey FOREIGN KEY (recipes_id) REFERENCES public.recipes(id) ON DELETE CASCADE;


--
-- Name: recipes recipes_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes
    ADD CONSTRAINT recipes_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: recipes recipes_recipes_categories_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipes
    ADD CONSTRAINT recipes_recipes_categories_id_fkey FOREIGN KEY (recipes_categories_id) REFERENCES public.recipes_categories(id) ON DELETE SET NULL;


--
-- Name: reviews reviews__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: reviews_attachments reviews_attachments_reviews_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_attachments
    ADD CONSTRAINT reviews_attachments_reviews_id_fkey FOREIGN KEY (reviews_id) REFERENCES public.reviews(id) ON DELETE CASCADE;


--
-- Name: reviews_comments reviews_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_comments
    ADD CONSTRAINT reviews_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: reviews_comments reviews_comments_reviews_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews_comments
    ADD CONSTRAINT reviews_comments_reviews_id_fkey FOREIGN KEY (reviews_id) REFERENCES public.reviews(id) ON DELETE CASCADE;


--
-- Name: reviews reviews_reviews_categories_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reviews
    ADD CONSTRAINT reviews_reviews_categories_id_fkey FOREIGN KEY (reviews_categories_id) REFERENCES public.reviews_categories(id) ON DELETE SET NULL;


--
-- Name: subscribers subscribers_mail_lists_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.subscribers
    ADD CONSTRAINT subscribers_mail_lists_id_fkey FOREIGN KEY (mail_lists_id) REFERENCES public.mail_lists(id) ON DELETE CASCADE;


--
-- Name: survey_answers survey_answers__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_answers
    ADD CONSTRAINT survey_answers__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: survey_answers survey_answers_survey_items_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_answers
    ADD CONSTRAINT survey_answers_survey_items_id_fkey FOREIGN KEY (survey_items_id) REFERENCES public.survey_items(id) ON DELETE CASCADE;


--
-- Name: survey_items survey_items_surveys_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_items
    ADD CONSTRAINT survey_items_surveys_id_fkey FOREIGN KEY (surveys_id) REFERENCES public.surveys(id) ON DELETE CASCADE;


--
-- Name: survey_questions survey_questions_surveys_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.survey_questions
    ADD CONSTRAINT survey_questions_surveys_id_fkey FOREIGN KEY (surveys_id) REFERENCES public.surveys(id) ON DELETE CASCADE;


--
-- Name: surveys surveys__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.surveys
    ADD CONSTRAINT surveys__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: tasks tasks__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tasks
    ADD CONSTRAINT tasks__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: timebank timebank__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank
    ADD CONSTRAINT timebank__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE SET NULL;


--
-- Name: timebank_comments timebank_comments__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank_comments
    ADD CONSTRAINT timebank_comments__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: timebank_comments timebank_comments_timebank_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank_comments
    ADD CONSTRAINT timebank_comments_timebank_id_fkey FOREIGN KEY (timebank_id) REFERENCES public.timebank(id) ON DELETE CASCADE;


--
-- Name: timebank timebank_recipient_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank
    ADD CONSTRAINT timebank_recipient_fkey FOREIGN KEY (recipient) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: timebank_requests timebank_requests__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.timebank_requests
    ADD CONSTRAINT timebank_requests__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: todos todos_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.todos
    ADD CONSTRAINT todos_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: transactions transactions_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transactions
    ADD CONSTRAINT transactions_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE SET NULL;


--
-- Name: user_roles user_roles_people_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_people_id_fkey FOREIGN KEY (people_id) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: user_roles user_roles_user_name_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_user_name_fkey FOREIGN KEY (user_name) REFERENCES public.people(user_name) ON UPDATE CASCADE;


--
-- Name: vehicles vehicles_families_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehicles
    ADD CONSTRAINT vehicles_families_id_fkey FOREIGN KEY (households_id) REFERENCES public.households(id) ON DELETE CASCADE;


--
-- Name: votes votes__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.votes
    ADD CONSTRAINT votes__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: votes votes_decisions_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.votes
    ADD CONSTRAINT votes_decisions_id_fkey FOREIGN KEY (decisions_id) REFERENCES public.decisions(id) ON DELETE CASCADE;


--
-- Name: work_hours work_hours__owner__fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work_hours
    ADD CONSTRAINT work_hours__owner__fkey FOREIGN KEY (_owner_) REFERENCES public.people(id) ON DELETE CASCADE;


--
-- Name: work_hours work_hours_work_tasks_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.work_hours
    ADD CONSTRAINT work_hours_work_tasks_id_fkey FOREIGN KEY (work_tasks_id) REFERENCES public.work_tasks(id) ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: sean
--

GRANT ALL ON SCHEMA public TO postgres;


--
-- PostgreSQL database dump complete
--

